#ifndef GLOBAL_H
#define GLOBAL_H

#include <cstring>
#include <singleton.h> // 线程安全的单例类实现
#include <common.h>

#include "audio_g711.h"
#include "aac_encode.h"

static bool g_isupdata = false;

#endif // GLOBAL_H
