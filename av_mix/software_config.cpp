#include "software_config.h"

SoftwareConfig::SoftwareConfig()
{
    // 初始化 向量类型softwareConfig,定义其长度为kSoftWareConfigIDMax，内容都先置为空
    configvalue_.assign(kSoftWareConfigIDMax, "");

    configvalue_[kIp]           = "128.0.110.20";
    configvalue_[kMask]         = "255.255.255.0";
    configvalue_[kGateway]      = "128.0.110.1";
    configvalue_[kWebPort]      = "80";
    configvalue_[kUdpSvPort]    = "12345";
    configvalue_[kRtspUri]      = "";
    configvalue_[kAiSampleRate] = "48000";
    configvalue_[kAiCoderFormat]  = "0"; //0:AAC; 1:AAC+; 2:AAC++; 3:G711A
    configvalue_[kAiBitRate]    = "128000";
    configvalue_[kSvRtspUri]    = "rtsp://128.0.110.20/0";
    configvalue_[kVI_W]         = "1280";
    configvalue_[kVI_H]         = "720";
    configvalue_[kVENC_Type]    = "96"; // 96:h264; 26:jpeg
    configvalue_[kGop]          = "30";
    configvalue_[kBitRate]      = "8000";
    configvalue_[kRcMode]       = "0"; /*0: CBR; 1: VBR; 2: FIXQP*/
    configvalue_[kProfile]      = "0"; /*0: baseline; 1:MP; 2:HP 3:svc-t */
    configvalue_[kMIN_QP]       = "23";
    configvalue_[kMAX_QP]       = "25";
    configvalue_[kI_QP]         = "0";
    configvalue_[kP_QP]         = "0";
    configvalue_[kDstFrmRate]   = "15";
    configvalue_[kSnapLen]      = "1200";
}

SoftwareConfig::~SoftwareConfig()
{
    COMMON_PRT("SoftwareConfig Normal Exit!\n");
}

bool SoftwareConfig::ReadConfig()
{
    COMMON_PRT("read_config:%s\n", filepath_);

    read_ini ri( filepath_ );
    ri.find_value( "ip=",           configvalue_[kIp]           );
    ri.find_value( "mask=",         configvalue_[kMask]         );
    ri.find_value( "gateway=",      configvalue_[kGateway]      );
    ri.find_value( "web_port=",     configvalue_[kWebPort]      );
    ri.find_value( "udpsvport=" ,   configvalue_[kUdpSvPort]    );
    ri.find_value( "rtsp_uri=",     configvalue_[kRtspUri]      );
    ri.find_value( "ai_sprate=" ,   configvalue_[kAiSampleRate] );
    ri.find_value( "aicodformat=",  configvalue_[kAiCoderFormat]);
    ri.find_value( "aibitrate=",    configvalue_[kAiBitRate]    );
    ri.find_value( "svrtspuri=",    configvalue_[kSvRtspUri]    );
    ri.find_value( "vi_w=",         configvalue_[kVI_W]         );
    ri.find_value( "vi_h=",         configvalue_[kVI_H]         );
    ri.find_value( "venc_type=",    configvalue_[kVENC_Type]    );
    ri.find_value( "gop=",          configvalue_[kGop]          );
    ri.find_value( "bit_rate=",     configvalue_[kBitRate]      );
    ri.find_value( "rcmode=",       configvalue_[kRcMode]       );
    ri.find_value( "profile=",      configvalue_[kProfile]      );
    ri.find_value( "min_qp=",       configvalue_[kMIN_QP]       );
    ri.find_value( "max_qp=",       configvalue_[kMAX_QP]       );
    ri.find_value( "i_qp=",         configvalue_[kI_QP]         );
    ri.find_value( "p_qp=",         configvalue_[kP_QP]         );
    ri.find_value( "venc_framerate=",   configvalue_[kDstFrmRate]   );
    ri.find_value( "snap_len=",     configvalue_[kSnapLen]      );

    return true;
}

bool SoftwareConfig::SaveConfig()
{
    stringstream ss;
    ss
    // preview
    << "[matrix preview]" << "\n"
    << "ip="            << configvalue_[kIp]            << "\n"
    << "mask="          << configvalue_[kMask]          << "\n"
    << "gateway="       << configvalue_[kGateway]       << "\n"
    << "web_port="      << configvalue_[kWebPort]       << "\n" 
    << "udpsvport="     << configvalue_[kUdpSvPort]     << "\n"
    << "rtsp_uri="      << configvalue_[kRtspUri]       << "\n"
    << "ai_sprate="     << configvalue_[kAiSampleRate]  << "\n"
    << "aicodformat="   << configvalue_[kAiCoderFormat] << "\n"
    << "aibitrate="     << configvalue_[kAiBitRate]     << "\n"
    << "svrtspuri="     << configvalue_[kSvRtspUri]     << "\n"
    << "vi_w="          << configvalue_[kVI_W]          << "\n"
    << "vi_h="          << configvalue_[kVI_H]          << "\n"
    << "venc_type="     << configvalue_[kVENC_Type]     << "\n"
    << "gop="           << configvalue_[kGop]           << "\n"
    << "bit_rate="      << configvalue_[kBitRate]       << "\n"
    << "rcmode="        << configvalue_[kRcMode]        << "\n"
    << "profile="       << configvalue_[kProfile]       << "\n"
    << "min_qp="        << configvalue_[kMIN_QP]        << "\n"
    << "max_qp="        << configvalue_[kMAX_QP]        << "\n"
    << "i_qp="          << configvalue_[kI_QP]          << "\n"
    << "p_qp="          << configvalue_[kP_QP]          << "\n"
    << "venc_framerate="    << configvalue_[kDstFrmRate]    << "\n"
    << "snap_len="      << configvalue_[kSnapLen]       << "\n";

    FILE * f = fopen( filepath_, "wb" );
    if(f)
    {
        fwrite(ss.str().c_str(), 1, ss.str().size(), f);
        fclose(f);
        COMMON_PRT("save %s succeed\n", filepath_);
        return true;
    }
    else
    {
        COMMON_PRT("save %s failed\n", filepath_);
        return false;
    }
}

void SoftwareConfig::PrintConfig()
{
    cout << "[matrix preview]" << endl;
    cout << "ip="           << configvalue_[kIp]            << endl;
    cout << "mask="         << configvalue_[kMask]          << endl;
    cout << "gateway="      << configvalue_[kGateway]       << endl;
    cout << "web_port="     << configvalue_[kWebPort]       << endl;
    cout << "udpsvport="    << configvalue_[kUdpSvPort]     << endl;
    cout << "rtsp_uri="     << configvalue_[kRtspUri]       << endl;
    cout << "ai_sprate="    << configvalue_[kAiSampleRate]  << endl;
    cout << "aicodformat="  << configvalue_[kAiCoderFormat] << endl;
    cout << "aibitrate="    << configvalue_[kAiBitRate]     << endl;
    cout << "svrtspuri="    << configvalue_[kSvRtspUri]     << endl;
    cout << "vi_w="         << configvalue_[kVI_W]          << endl;
    cout << "vi_h="         << configvalue_[kVI_H]          << endl;
    cout << "venc_type="    << configvalue_[kVENC_Type]     << endl;
    cout << "gop="          << configvalue_[kGop]           << endl;
    cout << "bit_rate="     << configvalue_[kBitRate]       << endl;
    cout << "rcmode="       << configvalue_[kRcMode]        << endl;
    cout << "profile="      << configvalue_[kProfile]       << endl;
    cout << "min_qp="       << configvalue_[kMIN_QP]        << endl;
    cout << "max_qp="       << configvalue_[kMAX_QP]        << endl;
    cout << "i_qp="         << configvalue_[kI_QP]          << endl;
    cout << "p_qp="         << configvalue_[kP_QP]          << endl;
    cout << "venc_framerate="   << configvalue_[kDstFrmRate]    << endl;
    cout << "snap_len="     << configvalue_[kSnapLen]       << endl;
}

bool SoftwareConfig::SetConfig(const SoftWareConfigID kId, string value)
{
    if(kId < kSoftWareConfigIDMax)
        configvalue_[kId] = value;
    else
        return false;

    return true;
}

string SoftwareConfig::GetConfig(const SoftWareConfigID kId)
{
    if(kId < kSoftWareConfigIDMax)
        return configvalue_[kId];
    else if(kId == kSoftWareConfigIDMax){
        stringstream ss;
        ss
        << "[matrix preview]" << "\n"
        << "ip="            << configvalue_[kIp]            << "\n"
        << "mask="          << configvalue_[kMask]          << "\n"
        << "gateway="       << configvalue_[kGateway]       << "\n"
	<< "web_port="      << configvalue_[kWebPort]       << "\n"
        << "udpsvport="     << configvalue_[kUdpSvPort]     << "\n"
        << "rtsp_uri="      << configvalue_[kRtspUri]       << "\n"
        << "ai_sprate="     << configvalue_[kAiSampleRate]  << "\n"
        << "aicodformat="   << configvalue_[kAiCoderFormat] << "\n"
        << "aibitrate="     << configvalue_[kAiBitRate]     << "\n"
        << "svrtspuri="     << configvalue_[kSvRtspUri]     << "\n"
        << "vi_w="          << configvalue_[kVI_W]          << "\n"
        << "vi_h="          << configvalue_[kVI_H]          << "\n"
        << "venc_type="     << configvalue_[kVENC_Type]     << "\n"
        << "gop="           << configvalue_[kGop]           << "\n"
        << "bit_rate="      << configvalue_[kBitRate]       << "\n"
        << "rcmode="        << configvalue_[kRcMode]        << "\n"
        << "profile="       << configvalue_[kProfile]       << "\n"
        << "min_qp="        << configvalue_[kMIN_QP]        << "\n"
        << "max_qp="        << configvalue_[kMAX_QP]        << "\n"
        << "i_qp="          << configvalue_[kI_QP]          << "\n"
        << "p_qp="          << configvalue_[kP_QP]          << "\n"
        << "venc_framerate="    << configvalue_[kDstFrmRate]    << "\n"
        << "snap_len="      << configvalue_[kSnapLen]       << "\n";

        return ss.str();
    }
    else
        return "";
}

bool SoftwareConfig::LoadConfigToGlobal(bool bPrintThem)
{
    if(bPrintThem){

    }

    return true;
}
