#ifndef Software_Config_H
#define Software_Config_H

#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <iostream>

#include <readini.h>
#include "global.h"

using namespace std;

class SoftwareConfig
{
public:
    typedef enum {
        kIp      = 0,
        kMask,
        kGateway,
        kWebPort,
        kUdpSvPort,    //add udpserverport
        kRtspUri,
        kAiSampleRate,
        kAiCoderFormat,    //add ai coderformat
        kAiBitRate,        //add ai ai bitrate
        kSvRtspUri,   //rtsp server uri
        kVI_W,
        kVI_H,
        kVENC_Type,
        kGop,       // 关键帧
        kRcMode,    // 码流控制
        kProfile,   // 编码质量等级
        kBitRate,   // 比特率
        kMIN_QP,
        kMAX_QP,
        kI_QP,      // 越小画值越高，与P_QP相差越小，呼吸效应越小
        kP_QP,
        kDstFrmRate,// 编码帧率
        kSnapLen,   // 回显手动拆包后每包大小
        kSoftWareConfigIDMax,
    }SoftWareConfigID;

    SoftwareConfig();
    ~SoftwareConfig();

    bool ReadConfig();
    bool SaveConfig();
    void PrintConfig();
    bool LoadConfigToGlobal(bool bPrintThem = false);
    bool SetConfig(const SoftWareConfigID kId, string value);
    string GetConfig(const SoftWareConfigID kId);

private:
    vector<string> configvalue_;
    const char *const filepath_ = "./media.ini"; //const data,const pointer
};

#endif // Software_Config_H
