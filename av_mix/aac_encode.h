#include <stdio.h>
#include <string.h>

#include "mpi_aenc.h"
#include "mpi_adec.h"
#include "hi_comm_aenc.h"
#include "hi_comm_adec.h"
#include "hi_comm_aio.h"
#include "aacenc.h"

/*samples per frame for AACLC and aacPlus */
#define AACLC_SAMPLES_PER_FRAME         1024  
#define AACPLUS_SAMPLES_PER_FRAME       2048

/*max length of AAC stream by bytes */
#define MAX_AAC_MAINBUF_SIZE    768*2

inline HI_S32 AencAACCheckConfig(AACENC_CONFIG *pconfig)
{
    if(NULL == pconfig)
    {
        return HI_ERR_AENC_NULL_PTR;
    }

    if(pconfig->coderFormat != AACLC && pconfig->coderFormat!= EAAC && pconfig->coderFormat != EAACPLUS)
    {
        printf("aacenc coderFormat(%d) invalid\n", pconfig->coderFormat);
		pconfig->coderFormat = AACLC;
    }


    if(pconfig->quality != AU_QualityExcellent && pconfig->quality!= AU_QualityHigh && pconfig->quality != AU_QualityMedium && pconfig->quality != AU_QualityLow) 
    {
        printf("aacenc quality(%d) invalid\n",pconfig->quality);
    }

    if(pconfig->bitsPerSample != 16) 
    {
        printf("aacenc bitsPerSample(%d) should be 16\n",pconfig->bitsPerSample);
		pconfig->bitsPerSample = 16;
    }

    if(pconfig->nChannelsIn != 2) 
    {
        printf("aacenc ChannelsIn(%d) should be 2\n",pconfig->nChannelsIn);
		pconfig->nChannelsIn = 2;
    }

    if(pconfig->coderFormat == AACLC)
    {
        if(pconfig->nChannelsOut != 2) 
        {
    	    printf("AACLC nChannelsOut(%d) should be 1\n",pconfig->nChannelsOut);
    	    pconfig->nChannelsOut = 2;
        }
    	
        if(pconfig->sampleRate == 32000)
        {
    	    if(pconfig->bitRate < 24000 ||  pconfig->bitRate > 256000)
    	    {
    		    printf("AACLC 32000 Hz bitRate(%d) should not be 24000 ~ 256000\n",pconfig->bitRate);
    		    pconfig->bitRate = 128000;
    	    }
        }
        else if(pconfig->sampleRate == 44100)
        {
    	    if(pconfig->bitRate < 32000 ||  pconfig->bitRate > 320000)
    	    {
    		    printf("AACLC 44100 Hz bitRate(%d) should not be 32000 ~ 320000\n",pconfig->bitRate);
    		    pconfig->bitRate = 128000;
    	    }
        }
        else if(pconfig->sampleRate == 48000)
        {
    	    if(pconfig->bitRate < 48000 ||  pconfig->bitRate > 320000)
    	    {
    		    printf("AACLC 32000 Hz bitRate(%d) should not be 48000 ~ 320000\n",pconfig->bitRate);
    		    pconfig->bitRate = 128000;
    	    }
        }
        else if(pconfig->sampleRate == 16000)
        {
    	    if(pconfig->bitRate < 16000 ||  pconfig->bitRate > 96000)
    	    {
    		    printf("AACLC 16000 Hz bitRate(%d) should not be 16000 ~ 96000\n",pconfig->bitRate);
    		    pconfig->bitRate = 64000;
    	    }
        }
        else if(pconfig->sampleRate == 24000)
        {
    	    if(pconfig->bitRate < 20000 ||  pconfig->bitRate > 128000)
    	    {
    		    printf("AACLC 24000 Hz bitRate(%d) should not be 20000 ~ 128000\n",pconfig->bitRate);
    		    pconfig->bitRate = 64000;
    	    }
        }
        else if(pconfig->sampleRate == 22050)
        {
    	    if(pconfig->bitRate < 16000 ||  pconfig->bitRate > 128000)
    	    {
    		    printf("AACLC 22025 Hz bitRate(%d) should not be 16000 ~ 128000\n",pconfig->bitRate);
    		    pconfig->bitRate = 64000;
    	    }
        }
        else
        {
    	    printf("AACLC invalid samplerate(%d) should be 16000~48000\n",pconfig->sampleRate);
    	    return HI_ERR_AENC_ILLEGAL_PARAM;
        }
    }   
    else if(pconfig->coderFormat == EAAC)
    {
        if(pconfig->nChannelsOut != 2) 
        {
    	    printf("EAAC nChannelsOut(%d) should be 1\n",pconfig->nChannelsOut);
    	    pconfig->nChannelsOut = 2;
        }
    	
        if(pconfig->sampleRate == 32000)
        {
    	    if(pconfig->bitRate < 18000 ||  pconfig->bitRate > 23000)
    	    {
    		    printf("EAAC 32000 Hz bitRate(%d) should be 18000 ~ 23000\n",pconfig->bitRate);
    		    pconfig->bitRate = 22000;
    	    }
        }
        else if(pconfig->sampleRate == 44100)
        {
    	    if(pconfig->bitRate < 24000 ||  pconfig->bitRate > 51000)
    	    {
    		    printf("EAAC 44100 Hz bitRate(%d) should be 24000 ~ 51000\n",pconfig->bitRate);
    		    pconfig->bitRate = 48000;
    	    }
        }
        else if(pconfig->sampleRate == 48000)
        {
    	    if(pconfig->bitRate < 24000 || pconfig->bitRate > 51000)
    	    {
    		    printf("EAAC 48000 Hz bitRate(%d) should be 24000 ~ 51000\n",pconfig->bitRate);
    		    pconfig->bitRate = 48000;
    	    }
        }
        else
        {
    	    printf("EAAC invalid samplerate(%d) should be 32000/44100/48000\n",pconfig->sampleRate);
    	    return HI_ERR_AENC_ILLEGAL_PARAM;
        }
    }  
    else if(pconfig->coderFormat == EAACPLUS)
    {
        if(pconfig->nChannelsOut != 1) 
        {
            printf("EAACPLUS nChannelsOut(%d) should be 1\n",pconfig->nChannelsOut);
            pconfig->nChannelsOut = 1;
        }
        
        if(pconfig->sampleRate == 32000)
        {
        	if(pconfig->bitRate < 10000 ||  pconfig->bitRate > 17000)
        	{
        	    printf("EAACPLUS 32000 Hz bitRate(%d) should  be 10000 ~ 17000\n",pconfig->bitRate);
        	    pconfig->bitRate = 16000;
        	}
        }
        else if(pconfig->sampleRate == 44100)
        {
        	if(pconfig->bitRate < 10000 ||  pconfig->bitRate > 17000)
        	{
        	    printf("EAACPLUS 44100 Hz bitRate(%d) should  be 10000 ~ 17000\n", pconfig->bitRate);
        	    pconfig->bitRate = 16000;
        	}
        }
        else if(pconfig->sampleRate == 48000)
        {
        	if(pconfig->bitRate < 12000 ||  pconfig->bitRate > 35000)
        	{
        	    printf("EAACPLUS 48000 Hz bitRate(%d) should  be 12000 ~ 35000\n",pconfig->bitRate);
        	    pconfig->bitRate = 32000;
        	}
        }
        else
        {
        	printf("EAACPLUS invalid samplerate(%d) should be 32000/44100/48000\n",pconfig->sampleRate);
        	return HI_ERR_AENC_ILLEGAL_PARAM;
        }
    }

    return 0;
}

class aacenc{
private:
	AAC_ENCODER_S * m_pstAACState;
	AuEncoderFormat m_coderFormat;
	HI_S32 m_sampleRate;
	HI_S32 m_bitRate;

public:
	aacenc() 
		: m_pstAACState(0)
		, m_coderFormat(AACLC)
		, m_sampleRate(48000)
		, m_bitRate(128000)
	{
	}

	~aacenc()
	{
		close();
	}

	void reopen()
	{
        close();
        open(m_bitRate, m_sampleRate, m_coderFormat);
	}

	HI_S32 open(HI_S32 bitRate, HI_S32 sampleRate, AuEncoderFormat coderFormat)
	{
		if(m_pstAACState)return 0;

		m_coderFormat = coderFormat;
		m_sampleRate = sampleRate;
		m_bitRate = bitRate;

		AACENC_CONFIG config;
		HI_S32 s32Ret = AACInitGetDefaultConfig(&config, m_coderFormat);
		if (s32Ret)return s32Ret;
    
        //config.quality      = AU_QualityExcellent;
		config.bitsPerSample = 16;
		config.sampleRate    = sampleRate;
		config.bitRate       = bitRate;
		config.nChannelsIn   = 2;
/*
		printf("config.bandWidth=%d\n", config.bandWidth);
		printf("config.quality=%d\n", config.quality);
		printf("config.nChannelsOut=%d\n", config.nChannelsOut);
		printf("config.coderFormat=%d\n", config.coderFormat);
		printf("config.sampleRate=%d\n", config.sampleRate);
*/
		s32Ret = AencAACCheckConfig(&config);
		if (s32Ret)return s32Ret;

		s32Ret = AACCheckConfig(&config);
		if (s32Ret)return s32Ret;

		return AACEncoderOpen(&m_pstAACState, &config);
	}

	void close()
	{
		if(m_pstAACState){
			AACEncoderClose(m_pstAACState);
			m_pstAACState = 0;
		}
	}

	HI_S32 enc(HI_U8 * pu8Left, HI_U8 * pu8Right, HI_U32 u32LROfBytes, HI_U8 *pu8Outbuf, HI_U32 *pu32OutLen)
	{
		HI_S16 aData[MAX_AUDIO_FRAME_LEN];
		HI_S16 s16Frames = u32LROfBytes/sizeof(HI_S16);

		short * l = (short *)pu8Left;
		short * r = (short *)pu8Right;
 
		/* AAC encoder need interleaved data,here change LLLRRR to LRLRLR. 
		   AACLC will encode 1024*2 point, and AACplus encode 2048*2 point*/         
		for (HI_S32 i = 0; i < s16Frames ; ++i)
		{
			memcpy(&aData[2*i], &l[i], sizeof(short));
			memcpy(&aData[2*i+1], &r[i], sizeof(short));
		}

		return AACEncoderFrame(m_pstAACState, aData, pu8Outbuf, (HI_S32*)pu32OutLen);
	}
};
