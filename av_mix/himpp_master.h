#ifndef HIMPP_MASTER_H
#define HIMPP_MASTER_H

#include <stdlib.h>

//#include "global.h"
#include "udpsocket/udpsocket.h"
#include "hi3520dv300_comm_fun.h"

#define SNAP_LEN_MAX 1200

class HimppMaster
{
public:
    HimppMaster();
    ~HimppMaster();

    bool InitHimpp(HI_S32 s32GrpCnt, PAYLOAD_TYPE_E enType, PIC_SIZE_E enPicSize);

    bool VpssModuleInit();
    bool VencModuleInit(VENC_CHN VencChn, SIZE_S stVencSize, HI_U32 u32Profile, HI_U32 u32Gop, HI_U32 u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 u32DstFrameRate, HI_U32 u32MinQp, HI_U32 u32MaxQp, HI_U32 u32IQp, HI_U32 u32PQp, HI_BOOL bEnSnap = HI_FALSE);
    bool VdecModuleInit(HI_U32 u32VdCnt, HI_BOOL isRotate = HI_FALSE);
    bool VoModeuleInit(VO_DEV VoDev, VO_LAYER VoLayer);
    bool WbcModuleInit(SIZE_S stSize);
    bool AiModeuleInit(AUDIO_SAMPLE_RATE_E enAiSampleRate);
    bool AencModeuleInit();
    bool StartAudioEnc();

    bool ModeuleBind();
    bool SetDivisionFactor(const char *ref_fb, const char *div12);
    bool SetVoSize(HI_U32 u32VoChn, RECT_S stRect, HI_U32 u32Priority);
    bool DisableVo(HI_U32 u32VoChn, HI_U32 u32Priority);

    bool GetVencSnap(UDPSocket *socket, const char* clientip, const HI_U32 clientport, HI_U32 snaplen);
    bool StartLoop();

    bool VdecModuleDestroy();
    bool VencModuleDestroy();
    bool VoModuleDestroy();
    bool VpssModuleDestroy();
    bool AudioModuleDestroy();

private:
    VIDEO_NORM_E enNorm_    = VIDEO_ENCODING_MODE_NTSC;
    SAMPLE_RC_E enRcMode_   = SAMPLE_RC_CBR;
    VDEC_CHN VdChn_ = 0;  //用于解码采集到的视频，并且作90度旋转
    HI_S32   VpssChnCnt_ = 1; //vpss的chn固定使用第0号通道，故个数为1
    HI_S32   VpssGrpCnt_; // = 5;
    VO_LAYER VoLayer_;  // = SAMPLE_VO_LAYER_VHD0;
    VO_DEV   VoDev_;    // = SAMPLE_VO_DEV_DHD0;
    VENC_CHN VencChn_;  // = 0;
    VENC_CHN VencSnapChn_;  // = 1;
    HI_U32   VdChnCnt_; // = 1;
    HI_U32   VoChn_;    // = 0;
    PAYLOAD_TYPE_E  enPayloadType_ = PT_LPCM;
    HI_BOOL         bUserGetMode_ = HI_FALSE;
    AUDIO_DEV       AiDev_ = SAMPLE_AUDIO_AI_DEV;
    HI_S32 s32AiChnCnt_;
    HI_S32 s32AencChnCnt_;

    SIZE_S stMaxSize_;
    SIZE_S stSnapSize_;
    PAYLOAD_TYPE_E enType_;

private:
    typedef struct snap_data
    {
        HI_U32   u32TotalPack;  //总包数
        HI_U32   u32CurrPack;   //当前包数
        HI_U8    pData[SNAP_LEN_MAX]; //图片信息
    }SNAP_DATA;
    bool SendSnap(HI_U8 *pu8Data, HI_U32 u32TotalLen, HI_U32 u32PackLen, UDPSocket *socket, const char* clientip, const uint clientport);
};

#endif // HIMPP_MASTER_H
