#include "himpp_master.h"

HimppMaster::HimppMaster()
{

}

HimppMaster::~HimppMaster()
{
    SAMPLE_COMM_SYS_Exit();
}

bool HimppMaster::InitHimpp(HI_S32 s32GrpCnt, PAYLOAD_TYPE_E enType, PIC_SIZE_E enPicSize)
{
    enType_ = enType;
    VpssGrpCnt_ = s32GrpCnt;

    if (HI_SUCCESS !=
        SAMPLE_COMM_SYS_GetPicSize(enNorm_, enPicSize, &stMaxSize_)){
        COMMON_PRT("SAMPLE_COMM_SYS_GetPicSize failed!\n");
        return false;
    }

    /******************************************
    step  1: init variable
    ******************************************/
    VB_CONF_S stVbConf;
    memset(&stVbConf, 0, sizeof(VB_CONF_S));
    stVbConf.u32MaxPoolCnt = 2;//VB_MAX_POOLS;
    stVbConf.astCommPool[0].u32BlkSize = ALIGN_UP(stMaxSize_.u32Width, SAMPLE_SYS_ALIGN_WIDTH) * ALIGN_UP(stMaxSize_.u32Height, SAMPLE_SYS_ALIGN_WIDTH) * 2;
    stVbConf.astCommPool[0].u32BlkCnt  = VpssGrpCnt_ * 12;
    memset(stVbConf.astCommPool[0].acMmzName, 0,
            sizeof(stVbConf.astCommPool[0].acMmzName));

    /******************************************
    step 2: mpp system init.
    ******************************************/
    if (HI_SUCCESS != SAMPLE_COMM_SYS_Init(&stVbConf)){
        COMMON_PRT("system init failed\n");
        system("reboot\n"); //保证程序不会进入无限循环的重启
    }

    return true;
}

bool HimppMaster::VpssModuleInit()
{
    HI_S32 s32Ret;
    VPSS_GRP_ATTR_S stGrpAttr;

    memset(&stGrpAttr,0,sizeof(VPSS_GRP_ATTR_S));
    stGrpAttr.u32MaxW   = stMaxSize_.u32Width;
    stGrpAttr.u32MaxH   = stMaxSize_.u32Height;
    stGrpAttr.enPixFmt  = SAMPLE_PIXEL_FORMAT;
    stGrpAttr.bIeEn     = HI_FALSE;
    stGrpAttr.bNrEn     = HI_TRUE;
    stGrpAttr.bDciEn    = HI_FALSE;
    stGrpAttr.bHistEn   = HI_FALSE;
    stGrpAttr.bEsEn     = HI_FALSE;
    stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
    s32Ret = SAMPLE_COMM_VPSS_Start(VpssGrpCnt_, &stMaxSize_, VpssChnCnt_, NULL);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("Start Vpss failed!\n");
        return false;
    }

    return true;
}

bool HimppMaster::VencModuleInit(VENC_CHN VencChn, SIZE_S stVencSize, HI_U32 u32Profile, HI_U32 u32Gop, HI_U32 u32BitRate, HI_U32 u32SrcFrmRate,  HI_U32 u32DstFrameRate, HI_U32 u32MinQp, HI_U32 u32MaxQp, HI_U32 u32IQp, HI_U32 u32PQp, HI_BOOL bEnSnap)
{
    HI_S32 s32Ret;
    // 编解码后用来在屏幕上输出的
    VencChn_ = VencChn;
    s32Ret = SAMPLE_COMM_VENC_Start(VencChn_, enType_, stMaxSize_,
                                    stVencSize, enRcMode_, u32Profile,
                                    u32Gop, u32BitRate, u32SrcFrmRate,
                                    u32DstFrameRate, u32MinQp, u32MaxQp,
                                    u32IQp, u32PQp);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("Start Venc[%d] failed!\n", VencChn);
        return false;
    }

    if(bEnSnap){ // 用于预览抓图
        VencSnapChn_ = VencChn + 1;
        stSnapSize_ = SIZE_S{240,180};
        s32Ret = SAMPLE_COMM_VENC_Start(VencSnapChn_, PT_JPEG, stMaxSize_,
                                        stSnapSize_, (SAMPLE_RC_E)0, 0,
                                        0, 0, 0,
                                        0, 0, 0,
                                        0, 0);
        if (HI_SUCCESS != s32Ret){
            COMMON_PRT("Start Venc[%d] failed!\n", VencSnapChn_);
            return false;
        }
    }

    return true;
}

bool HimppMaster::VdecModuleInit(HI_U32 u32VdCnt, HI_BOOL isRotate)
{
    /************************************************
    vdec: step2:  init mod common VB
    *************************************************/
    HI_S32 s32Ret;
    VB_CONF_S stModVbConf;
    VDEC_CHN_ATTR_S stVdecChnAttr;
    SAMPLE_COMM_VDEC_ModCommPoolConf(&stModVbConf, enType_, &stMaxSize_, u32VdCnt);
    s32Ret = SAMPLE_COMM_VDEC_InitModCommVb(&stModVbConf);
    if(s32Ret != HI_SUCCESS){
        COMMON_PRT("init mod common vb fail for %#x!\n", s32Ret);
        return false;
    }

    /************************************************
    vdec: step3:  start VDEC
    *************************************************/
    VdChnCnt_ = u32VdCnt;
    for(HI_U32 i = 0; i < u32VdCnt; i++){
        SAMPLE_COMM_VDEC_ChnAttr(i, &stVdecChnAttr, enType_, &stMaxSize_);
        s32Ret = SAMPLE_COMM_VDEC_Start(i, &stVdecChnAttr);
        if(s32Ret != HI_SUCCESS){
            COMMON_PRT("start VDEC fail for %#x!\n", s32Ret);
            return false;
        }
    }

    if(isRotate){
        s32Ret = HI_MPI_VDEC_SetRotate(VdChn_, ROTATE_90);
        if(s32Ret != HI_SUCCESS){
            COMMON_PRT("HI_MPI_VDEC_SetRotate fail for %#x!\n", s32Ret);
            return false;
        }
    }

    return true;
}

bool HimppMaster::VoModeuleInit(VO_DEV VoDev, VO_LAYER VoLayer)
{
    VO_PUB_ATTR_S stVoPubAttr;
    VO_VIDEO_LAYER_ATTR_S stLayerAttr;
    VoDev_   = VoDev;
    VoLayer_ = VoLayer;

#if 1
    stVoPubAttr.enIntfSync = VO_OUTPUT_1080P60;
#else
    stVoPubAttr.enIntfSync = VO_OUTPUT_USER;
    stVoPubAttr.stSyncInfo.bSynm   = HI_FALSE;
    stVoPubAttr.stSyncInfo.bIop    = HI_TRUE;
    stVoPubAttr.stSyncInfo.u8Intfb = 0;
    stVoPubAttr.stSyncInfo.u16Vact = 854;
    stVoPubAttr.stSyncInfo.u16Vbb  = 30;
    stVoPubAttr.stSyncInfo.u16Vfb  = 886-854-30;
    stVoPubAttr.stSyncInfo.u16Hact = 480;
    stVoPubAttr.stSyncInfo.u16Hbb  = 120;
    stVoPubAttr.stSyncInfo.u16Hfb  = 624-480-120;
    stVoPubAttr.stSyncInfo.u16Hmid = 0;
    stVoPubAttr.stSyncInfo.u16Bvact= 0;
    stVoPubAttr.stSyncInfo.u16Bvbb = 0;
    stVoPubAttr.stSyncInfo.u16Bvfb = 0;
    stVoPubAttr.stSyncInfo.u16Hpw  = 62;
    stVoPubAttr.stSyncInfo.u16Vpw  = 6;
    stVoPubAttr.stSyncInfo.bIdv    = HI_FALSE;
    stVoPubAttr.stSyncInfo.bIhs    = HI_TRUE;
    stVoPubAttr.stSyncInfo.bIvs    = HI_TRUE;
#if 0 //阿美林 屏幕驱动参数
    stVoPubAttr.stSyncInfo.u16Vact = 854;
    stVoPubAttr.stSyncInfo.u16Vbb  = 36;
    stVoPubAttr.stSyncInfo.u16Vfb  = 10;
    stVoPubAttr.stSyncInfo.u16Hact = 480;
    stVoPubAttr.stSyncInfo.u16Hbb  = 198;
    stVoPubAttr.stSyncInfo.u16Hfb  = 10;
    stVoPubAttr.stSyncInfo.bIhs    = HI_FALSE;
    stVoPubAttr.stSyncInfo.bIvs    = HI_FALSE;
#endif
#endif
    stVoPubAttr.enIntfType = VO_INTF_HDMI;
    stVoPubAttr.u32BgColor = 0x000000ff;

    HI_S32 s32Ret;
    s32Ret = SAMPLE_COMM_VO_StartDev(VoDev, &stVoPubAttr);
    if (HI_SUCCESS != s32Ret)
    {
        COMMON_PRT("Start SAMPLE_COMM_VO_StartDev failed!\n");
        return false;
    }

    memset(&(stLayerAttr), 0 , sizeof(VO_VIDEO_LAYER_ATTR_S));
    s32Ret = SAMPLE_COMM_VO_GetWH(stVoPubAttr.enIntfSync, \
                                  &stLayerAttr.stImageSize.u32Width, \
                                  &stLayerAttr.stImageSize.u32Height, \
                                  &stLayerAttr.u32DispFrmRt);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("Start SAMPLE_COMM_VO_GetWH failed!\n");
        return false;
    }

    stLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
    stLayerAttr.stDispRect.s32X 	  = 0;
    stLayerAttr.stDispRect.s32Y 	  = 0;
    stLayerAttr.stDispRect.u32Width   = stLayerAttr.stImageSize.u32Width;
    stLayerAttr.stDispRect.u32Height  = stLayerAttr.stImageSize.u32Height;
    s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer_, &stLayerAttr);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("Start SAMPLE_COMM_VO_StartLayer failed!\n");
        return false;
    }

    if (stVoPubAttr.enIntfType & VO_INTF_HDMI){
        if (HI_SUCCESS != SAMPLE_COMM_VO_HdmiStart(stVoPubAttr.enIntfSync)){
            COMMON_PRT("Start SAMPLE_COMM_VO_HdmiStart failed!\n");
            return false;
        }
    }

    return true;
}

bool HimppMaster::WbcModuleInit(SIZE_S stSize)
{
    HI_S32          s32Ret;
    VO_WBC          VoWbc = SAMPLE_VO_WBC_BASE;
    VO_WBC_ATTR_S   stWbcAttr;
    VO_WBC_MODE_E   enWbcMode;
    VO_WBC_SOURCE_S stWbcSource;

    stWbcSource.enSourceType = VO_WBC_SOURCE_DEV;
    stWbcSource.u32SourceId  = VoDev_;
    COMMON_PRT("stWbcSource.enSourceType: %d, "
               "stWbcSource.u32SourceId: %d\n",
               stWbcSource.enSourceType, stWbcSource.u32SourceId);

    s32Ret = SAMPLE_COMM_WBC_BindVo(VoWbc, &stWbcSource);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("SAMPLE_COMM_WBC_BindVo failed with 0x%x!\n", s32Ret);
        return false;
    }

    stWbcAttr.stTargetSize.u32Width  = stSize.u32Width;
    stWbcAttr.stTargetSize.u32Height = stSize.u32Height;
    stWbcAttr.enPixelFormat = PIXEL_FORMAT_YUV_SEMIPLANAR_420;
    stWbcAttr.u32FrameRate  = 30;

    s32Ret = HI_MPI_VO_SetWbcAttr(VoDev_, &stWbcAttr);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("HI_MPI_VO_SetWbcAttr failed with 0x%x!\n", s32Ret);
        return false;
    }

    //(VO_WBC_MODE_E)0 cpp
    s32Ret = HI_MPI_VO_GetWbcMode(VoDev_, &enWbcMode);
    if (HI_SUCCESS !=s32Ret){
        COMMON_PRT("HI_MPI_VO_GetWbcMode failed with: 0x%x\n", s32Ret);
    }
    COMMON_PRT("Get wbc mode %d", enWbcMode);

    enWbcMode = VO_WBC_MODE_NOMAL; //该模式下，仅根据设备帧率和回写帧率来控制采集图像
    s32Ret = HI_MPI_VO_SetWbcMode(VoDev_, enWbcMode);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("HI_MPI_VO_SetWbcMode failed with: 0x%x\n", s32Ret);
        return false;
    }

    /* enable wbc */
    s32Ret = HI_MPI_VO_EnableWbc(VoDev_);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("HI_MPI_VO_EnableWbc failed with: 0x%x\n", s32Ret);
        return false;
    }

    s32Ret = HI_MPI_VO_SetWbcDepth(VoDev_, 1);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("HI_MPI_VO_SetWbcDepth failed with: 0x%x\n", s32Ret);
        return false;
    }

    return true;
}

bool HimppMaster::AiModeuleInit(AUDIO_SAMPLE_RATE_E enAiSampleRate)
{
    AIO_ATTR_S stAioAttr;
    stAioAttr.enSamplerate   = enAiSampleRate;
    stAioAttr.enBitwidth     = AUDIO_BIT_WIDTH_16;
    stAioAttr.enWorkmode     = AIO_MODE_I2S_MASTER;
    stAioAttr.enSoundmode    = AUDIO_SOUND_MODE_MONO;
    stAioAttr.u32EXFlag      = 1;
    stAioAttr.u32FrmNum      = 30;
    stAioAttr.u32PtNumPerFrm = SAMPLE_AUDIO_PTNUMPERFRM;
    stAioAttr.u32ChnCnt      = 1;
    stAioAttr.u32ClkChnCnt   = 2;
    stAioAttr.u32ClkSel      = 0;

    /********************************************
    step 2: start Ai
    ********************************************/
    AI_VQE_CONFIG_S pstAiVqeAttr;
    pstAiVqeAttr.bHpfOpen = HI_FALSE;
    pstAiVqeAttr.bRnrOpen = HI_TRUE;
    pstAiVqeAttr.bAecOpen = HI_FALSE;
    pstAiVqeAttr.bAnrOpen = HI_FALSE;
    pstAiVqeAttr.bAgcOpen = HI_FALSE;
    pstAiVqeAttr.bEqOpen = HI_FALSE;
    pstAiVqeAttr.bHdrOpen = HI_FALSE;
    pstAiVqeAttr.enWorkstate = VQE_WORKSTATE_MUSIC; //VQE_WORKSTATE_COMMON,VQE_WORKSTATE_MUSIC,VQE_WORKSTATE_NOISY
    pstAiVqeAttr.s32WorkSampleRate = enAiSampleRate;
    pstAiVqeAttr.s32FrameSample = SAMPLE_AUDIO_PTNUMPERFRM;
    //pstAiVqeAttr->stHpfCfg.bUsrMode = HI_TRUE;//HI_FALSE;
    //pstAiVqeAttr->stHpfCfg.enHpfFreq = AUDIO_HPF_FREQ_120;
    //pstAiVqeAttr->stAnrCfg.bUsrMode = HI_TRUE;//HI_FALSE;
    //pstAiVqeAttr->stAnrCfg.s16NrIntensity = 25;
    //pstAiVqeAttr->stAnrCfg.s16NoiseDbThr = 60;
    //pstAiVqeAttr->stAnrCfg.s8SpProSwitch = 1;
    pstAiVqeAttr.stRnrCfg.bUsrMode = HI_TRUE;//HI_FALSE;
    pstAiVqeAttr.stRnrCfg.s32NrMode = 0;
    pstAiVqeAttr.stRnrCfg.s32MaxNrLevel = 20;
    pstAiVqeAttr.stRnrCfg.s32NoiseThresh = -30;
    //pstAiVqeAttr->stAgcCfg.bUsrMode = HI_FALSE;

    bUserGetMode_ = HI_TRUE;

    s32AiChnCnt_ = stAioAttr.u32ChnCnt;
    HI_S32 s32Ret = SAMPLE_COMM_AUDIO_StartAi(AiDev_, s32AiChnCnt_, &stAioAttr, AUDIO_SAMPLE_RATE_BUTT, HI_FALSE, &pstAiVqeAttr);
    //HI_S32 s32Ret = SAMPLE_COMM_AUDIO_StartAi(AiDev_, s32AiChnCnt_, &stAioAttr, AUDIO_SAMPLE_RATE_BUTT, HI_FALSE, 0);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_StartAi failed with %x\n", s32Ret);
        return HI_FALSE;
    }

    return true;
}

bool HimppMaster::AencModeuleInit()
{
    /********************************************
    step 3: start Aenc
    ********************************************/
    s32AencChnCnt_ = 1;
    HI_S32 s32Ret = SAMPLE_COMM_AUDIO_StartAenc(s32AencChnCnt_, SAMPLE_AUDIO_PTNUMPERFRM, enPayloadType_);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_StartAenc failed with %x\n", s32Ret);
        return HI_FALSE;
    }

    return true;
}

bool HimppMaster::StartAudioEnc()
{
    for(AENC_CHN AeChn = 0; AeChn < s32AencChnCnt_; AeChn++){
        HI_S32 s32Ret = SAMPLE_COMM_AUDIO_CreatTrdAencAdec(AeChn, -1, NULL);
        if (s32Ret != HI_SUCCESS){
            COMMON_PRT("SAMPLE_COMM_AUDIO_CreatTrdAencAdec failed with %x\n", s32Ret);
            return false;
        }
    }

    return true;
}

bool HimppMaster::GetVencSnap(UDPSocket *socket, const char *clientip, const HI_U32 clientport, HI_U32 snaplen)
{
    /* Set Venc Fd. */
    HI_S32 s32Ret;
    HI_S32 VencFd = HI_MPI_VENC_GetFd( VencSnapChn_ );
    if (VencFd < 0){
        COMMON_PRT("HI_MPI_VENC_GetFd failed with %#x!\n", VencFd);
        return false;
    }
    COMMON_PRT("Snap VencFd: %d\n", VencFd);

    fd_set read_fds;
    VENC_CHN_STAT_S stStat;
    VENC_STREAM_S stVencStream;
    struct timeval TimeoutVal;
    HI_U32 counter = 0;
    HI_U32 pic_counter = 0;
    while( counter < 5 ){
        FD_ZERO(&read_fds);
        FD_SET(VencFd, &read_fds);
        TimeoutVal.tv_sec  = 0;
        TimeoutVal.tv_usec = 500000;
        s32Ret = select(VencFd + 1, &read_fds, NULL, NULL, &TimeoutVal);
        if (s32Ret < 0){
            COMMON_PRT("select failed!\n");
            counter++;
            break;
        }
        else if (s32Ret == 0){
            COMMON_PRT("get venc stream time out, exit thread\n");
            counter++;
            continue;
        }
        else{
            if (FD_ISSET(VencFd, &read_fds)){
                s32Ret = HI_MPI_VENC_Query(VencSnapChn_, &stStat);
                if (HI_SUCCESS != s32Ret){
                    COMMON_PRT("HI_MPI_VENC_Query chn[%d] failed with %#x!\n", VencSnapChn_, s32Ret);
                    break;
                }
                if(0 == stStat.u32CurPacks){
                    COMMON_PRT("NOTE: Current frame is NULL!\n");
                    continue;
                }

                memset(&stVencStream, 0, sizeof(stVencStream));
                stVencStream.pstPack = (VENC_PACK_S*)malloc(sizeof(VENC_PACK_S) * stStat.u32CurPacks);
                if (NULL == stVencStream.pstPack){
                    COMMON_PRT("malloc stream pack failed!\n");
                    break;
                }

                //                COMMON_PRT("u32LeftPics: %d, u32LeftEncPics: %d, u32LeftRecvPics: %d\n",
                //                           stStat.u32LeftPics, stStat.u32LeftEncPics, stStat.u32LeftRecvPics);
                pic_counter = 0; //记录每次while循环中读取的图片张数
                stVencStream.u32PackCount = stStat.u32CurPacks;
                while(1){ //加上循环，为了读取缓存中全部张数，防止出现缓存过多的情况
                    s32Ret = HI_MPI_VENC_GetStream(VencSnapChn_, &stVencStream, HI_TRUE);
                    if (HI_SUCCESS != s32Ret){
                        free(stVencStream.pstPack);
                        stVencStream.pstPack = NULL;
                        COMMON_PRT("HI_MPI_VENC_GetStream failed with %#x!\n", s32Ret);
                        break;
                    }

                    //                    COMMON_PRT("pic_counter: %d\n", pic_counter);
                    if(pic_counter == 0){ //只对第一张图进行发送。
                        //TO DO:Send it
                        HI_U32 u32Len = 0;
                        for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++)
                            u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);

                        HI_U8 *pu8Data = (HI_U8*)malloc(u32Len);
                        u32Len = 0;
                        for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++){
                            memcpy(pu8Data + u32Len,
                                   stVencStream.pstPack[j].pu8Addr + stVencStream.pstPack[j].u32Offset,
                                   stVencStream.pstPack[j].u32Len - stVencStream.pstPack[j].u32Offset);
                            u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);
                        }
                        this->SendSnap(pu8Data, u32Len, snaplen, socket, clientip, clientport);
                        free(pu8Data);
                        //                        COMMON_PRT("send pic ok!\n");
                    }
                    pic_counter++;

                    s32Ret = HI_MPI_VENC_ReleaseStream(VencSnapChn_, &stVencStream);
                    if (HI_SUCCESS != s32Ret){
                        free(stVencStream.pstPack);
                        stVencStream.pstPack = NULL;
                        break;
                    }
                }

                free(stVencStream.pstPack);
                stVencStream.pstPack = NULL;
                break;
            }
        }
    }

    return true;
}

bool HimppMaster::SendSnap(HI_U8 *pu8Data, HI_U32 u32TotalLen, HI_U32 u32PackLen, UDPSocket *socket, const char* clientip, const uint clientport)
{
    SNAP_DATA snapdata;
    HI_S32 sendlen  = 0;

    //    snapdata.pData = new HI_U8[u32PackLen];
    snapdata.u32TotalPack = (u32TotalLen + u32PackLen - 1) / u32PackLen; //总包数

    for(HI_U32 i = 0; i < snapdata.u32TotalPack; i++){
        //        COMMON_PRT("u32TotalLen: %d, now: %d, totalPackNum: %d\n", u32TotalLen, i, snapdata.u32TotalPack);
        snapdata.u32CurrPack = i; //当前包序号
        memcpy(snapdata.pData, pu8Data, MIN2(u32PackLen, u32TotalLen));

        sendlen = socket->SendTo(clientip, clientport, (char*)&snapdata, u32PackLen + 2 * sizeof(HI_U32));
        //        COMMON_PRT("snap sendto len: %d\n", sendlen);

        u32TotalLen -= u32PackLen;
        pu8Data     += u32PackLen;
        //usleep(800);
    }

    //    delete[] snapdata.pData;

    return true;
}

bool HimppMaster::ModeuleBind()
{
#ifdef __TRANS_CODING__
    /*
     * VdecChn(0) >--VpssGrp(0)--> VencChn(0)
    */
    VPSS_GRP VpssGrp = 0;
    if(HI_SUCCESS != SAMPLE_COMM_VDEC_BindVpss(VdChn_, VpssGrp)){
        COMMON_PRT("Vdec bind Vpss failed\n");
        return false;
    }

    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencChn_, VpssGrp)){
        COMMON_PRT("Venc bind Vpss failed!\n");
        return false;
    }
#endif //__TRANS_CODING__

    /*
     * Ai(0) >--Aenc(0)
    */
    HI_S32   s32Ret;
    AI_CHN   AiChn = 0;
    AENC_CHN AeChn = 0;
    if (HI_TRUE == bUserGetMode_){
        s32Ret = SAMPLE_COMM_AUDIO_CreatTrdAiAenc(AiDev_, AiChn, AeChn);
        if (s32Ret != HI_SUCCESS){
            COMMON_PRT("SAMPLE_COMM_AUDIO_CreatTrdAiAenc failed with %x", s32Ret);
            return false;
        }
    }
    else{
        s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(AiDev_, AiChn, AeChn);
        if (s32Ret != HI_SUCCESS){
            COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
            return false;
        }
    }
    return true;
}

bool HimppMaster::SetVoSize(HI_U32 u32VoChn, RECT_S stRect, HI_U32 u32Priority)
{
    HI_S32 s32Ret = HI_SUCCESS;
    VO_CHN_ATTR_S stVoChnAttr;

    VoChn_ = u32VoChn;
    stVoChnAttr.stRect      = stRect;
    stVoChnAttr.u32Priority = u32Priority;
    stVoChnAttr.bDeflicker  = HI_FALSE;

    COMMON_PRT("Chn = %d, Pri = %d, vo_X = %d, vo_Y = %d, vo_W = %d, vo_H = %d\n", VoChn_, stVoChnAttr.u32Priority, stVoChnAttr.stRect.s32X, stVoChnAttr.stRect.s32Y, stVoChnAttr.stRect.u32Width, stVoChnAttr.stRect.u32Height);

    s32Ret = HI_MPI_VO_SetChnAttr(VoLayer_, VoChn_, &stVoChnAttr);
    if (s32Ret != HI_SUCCESS)
    {
        COMMON_PRT("HI_MPI_VO_SetChnAttr failed with %x!\n", s32Ret);
        return false;
    }

    s32Ret = HI_MPI_VO_EnableChn(VoLayer_, VoChn_);
    if (s32Ret != HI_SUCCESS)
    {
        COMMON_PRT("VO Enable Error = %x!\n", s32Ret);
        return false;
    }

    return true;
}

bool HimppMaster::DisableVo(HI_U32 VoChn_, HI_U32 u32Priority)
{
    HI_S32 s32Ret;
    VO_LAYER volayer = VoLayer_;

    if(u32Priority == 1)
        volayer = SAMPLE_VO_LAYER_VPIP;

    s32Ret = HI_MPI_VO_ClearChnBuffer(volayer, VoChn_, HI_TRUE);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("HI_MPI_VO_ClearChnBuffer failed!\n");
        return HI_FAILURE;
    }

    /*********************************************
     Stop vo & Unbind vo to vpss & DisablePipLayer
    **********************************************/
    s32Ret = HI_MPI_VO_DisableChn(volayer, VoChn_);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("HI_MPI_VO_DisableChn failed\n");
        return HI_FAILURE;
    }

    SAMPLE_COMM_VO_UnBindVpss(volayer, VoChn_, VoChn_);

    return true;
}

bool HimppMaster::SetDivisionFactor(const char *ref_fb, const char *div12)
{
    char cmd[128] = "";

    //VPLL0 第二级输出分频系数;VPLL0 第一级输出分频系数. 三星4K屏幕使用：0x54000000
    sprintf(cmd, "himm 0x12040008 %s", div12);
    system(cmd);

    // [17:12]VPLL0 参考时钟分频系数;[11:0]VPLL0 整数倍频系数. 原始0x03002063
    sprintf(cmd, "himm 0x1204000c %s", ref_fb);
    system(cmd);

    return true;
}

bool HimppMaster::VdecModuleDestroy()
{
    SAMPLE_COMM_VDEC_Stop(VdChnCnt_);

    return HI_SUCCESS;
}

bool HimppMaster::VencModuleDestroy()
{
    SAMPLE_COMM_VENC_Stop(VencChn_);

    return HI_SUCCESS;
}

bool HimppMaster::VoModuleDestroy()
{
    if(HI_SUCCESS != SAMPLE_COMM_VO_StopChn(VoLayer_, VoChn_))
        return HI_FAILURE;

    if(HI_SUCCESS != SAMPLE_COMM_VO_StopLayer(VoLayer_))
        return HI_FAILURE;

    if(HI_SUCCESS != SAMPLE_COMM_VO_StopDev(VoDev_))
        return HI_FAILURE;

    if(HI_SUCCESS != SAMPLE_COMM_VO_HdmiStop())
        return HI_FAILURE;

    return HI_SUCCESS;
}

bool HimppMaster::VpssModuleDestroy()
{
    if(HI_SUCCESS != SAMPLE_COMM_VPSS_Stop(VpssGrpCnt_, VpssChnCnt_)){
        COMMON_PRT("SAMPLE_COMM_VPSS_Stop failed!\n");
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

bool HimppMaster::AudioModuleDestroy()
{
    HI_S32   s32Ret;
    AI_CHN   AiChn = 0;
    AENC_CHN AeChn = 0;

    if (HI_TRUE == bUserGetMode_)
    {
        s32Ret = SAMPLE_COMM_AUDIO_DestoryTrdAi(AiDev_, AiChn);
        if (s32Ret != HI_SUCCESS)
        {
            COMMON_PRT("SAMPLE_COMM_AUDIO_DestoryTrdAi failed with %x", s32Ret);
            return HI_FAILURE;
        }
    }
    else
    {
        s32Ret = SAMPLE_COMM_AUDIO_AencUnbindAi(AiDev_, AiChn, AeChn);
        if (s32Ret != HI_SUCCESS)
        {
            COMMON_PRT("SAMPLE_COMM_AUDIO_AencUnbindAi failed with %x", s32Ret);
            return HI_FAILURE;
        }
    }
    s32Ret = SAMPLE_COMM_AUDIO_StopAenc(s32AencChnCnt_);
    if (s32Ret != HI_SUCCESS)
    {
        COMMON_PRT("SAMPLE_COMM_AUDIO_StopAenc failed with %x", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = SAMPLE_COMM_AUDIO_StopAi(AiDev_, s32AiChnCnt_, HI_FALSE, HI_FALSE);
    if (s32Ret != HI_SUCCESS)
    {
        COMMON_PRT("SAMPLE_COMM_AUDIO_StopAi failed with %x", s32Ret);
        return HI_FAILURE;
    }

    return true;
}

bool HimppMaster::StartLoop()
{
    /* Set Venc Fd. */
    HI_S32 s32Ret;
    HI_S32 VencFd = HI_MPI_VENC_GetFd( VencChn_);
    if (VencFd < 0){
        COMMON_PRT("HI_MPI_VENC_GetFd failed with %#x!\n", VencFd);
        return false;
    }
    COMMON_PRT("VencFd: %d\n", VencFd);

    fd_set read_fds;
    VENC_CHN_STAT_S stStat;
    VENC_STREAM_S stVencStream;
    struct timeval TimeoutVal;
    while( 1 ){
        FD_ZERO(&read_fds);
        FD_SET(VencFd, &read_fds);
        TimeoutVal.tv_sec  = 2;
        TimeoutVal.tv_usec = 0;
        s32Ret = select(VencFd + 1, &read_fds, NULL, NULL, &TimeoutVal);
        if (s32Ret < 0){
            COMMON_PRT("select failed!\n");
            break;
        }
        else if (s32Ret == 0){
            COMMON_PRT("get venc stream time out, exit thread\n");
            continue;
        }
        else{
            if (FD_ISSET(VencFd, &read_fds)){
                /***************************************************
                 step 2.1 : query how many packs in one-frame stream.
                ***************************************************/
                s32Ret = HI_MPI_VENC_Query(VencChn_, &stStat);
                if (HI_SUCCESS != s32Ret){
                    COMMON_PRT("HI_MPI_VENC_Query chn[%d] failed with %#x!\n", VencChn_, s32Ret);
                    break;
                }
                if(0 == stStat.u32CurPacks){
                    COMMON_PRT("NOTE: Current frame is NULL!\n");
                    continue;
                }
                /***************************************************
                 step 2.3 : malloc corresponding number of pack nodes.
                ***************************************************/
                memset(&stVencStream, 0, sizeof(stVencStream));
                stVencStream.pstPack = (VENC_PACK_S*)malloc(sizeof(VENC_PACK_S) * stStat.u32CurPacks);
                if (NULL == stVencStream.pstPack){
                    COMMON_PRT("malloc stream pack failed!\n");
                    break;
                }
                /***************************************************
                 step 2.4 : call mpi to get one-frame stream
                ***************************************************/
                stVencStream.u32PackCount = stStat.u32CurPacks;
                s32Ret = HI_MPI_VENC_GetStream(VencChn_, &stVencStream, HI_TRUE);
                if (HI_SUCCESS != s32Ret){
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                    COMMON_PRT("HI_MPI_VENC_GetStream failed with %#x!\n", s32Ret);
                    break;
                }

#ifdef __TINY_RTSP_SERVER__
                g_prtspserver->push_live_video_data( g_stream,
                                                     (void*)stVencStream.pstPack->pu8Addr,
                                                     stVencStream.pstPack->u32Len,
                                                     stVencStream.pstPack->u64PTS/1000 );
#else
                crtsps_pushvideo( g_stream,
                                  (void*)stVencStream.pstPack->pu8Addr,
                                  stVencStream.pstPack->u32Len,
                                  true,
                                  stVencStream.pstPack->u64PTS/1000 );
#endif //__TINY_RTSP_SERVER__

                /***************************************************
                 step 2.6 : release stream
                ***************************************************/
                s32Ret = HI_MPI_VENC_ReleaseStream(VencChn_, &stVencStream);
                if (HI_SUCCESS != s32Ret){
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                    break;
                }
                /***************************************************
                 step 2.7 : free pack nodes
                ***************************************************/
                free(stVencStream.pstPack);
                stVencStream.pstPack = NULL;
            }
        }
    }

    return false;
}
