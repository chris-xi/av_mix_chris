TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

##指定目标文件(obj)的存放目录
OBJECTS_DIR += ../obj

#头文件包含路径
INCLUDEPATH += /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/common \
               /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/rtspc \
               /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/rtsps
INCLUDEPATH += /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/include \
               /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/extdrv/nvp6124 \
               /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/extdrv/tlv320aic31 \
               /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/extdrv/tp2823

#引入的lib文件的路径  -L：引入路径
LIBS += /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/lib/libmpi.a \
        /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/lib/libhdmi.a \
        /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/lib/libive.a \
        /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/lib/libjpeg6b.a \
        /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/lib/libjpeg.a \
        /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/lib/libtde.a \
        /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/lib/libVoiceEngine.a \
        /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/lib/libupvqe.a \
        /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-hi3521a-sdk-v1.0.3.0/lib/libdnvqe.a \
        /home/roger/workspace/hi3520dv300_avMix/av_mix/av_mix/lib/arm-hisiv300-aacenc/libaacenc.a
LIBS += ./librtspc.so ./librtsps_v2.so #./librtsps.so
LIBS += -lpthread -lm -ldl -lrt \
        -lstdc++

#不检测定义后未使用的参数错误等
QMAKE_CXXFLAGS += -Wno-unused-parameter \
                  -Wno-unused-but-set-variable \
                  -Wno-unused-but-set-parameter \
                  -Wno-narrowing \
                  -Wno-literal-suffix \
                  -std=c++11 \
                  -mcpu=cortex-a7 \
                  -mfloat-abi=softfp \
                  -mfpu=neon-vfpv4 \
                  -mno-unaligned-access \
                  -fno-aggressive-loop-optimizations
QMAKE_CFLAGS += -Wno-unused-parameter \
                -Wno-unused-but-set-variable \
                -Wno-unused-but-set-parameter \
                -mcpu=cortex-a7 \
                -mfloat-abi=softfp \
                -mfpu=neon-vfpv4 \
                -mno-unaligned-access \
                -fno-aggressive-loop-optimizations

SOURCES += main.cpp \
    http_server.cpp \
    himpp_master.cpp \
    software_config.cpp \
    loadbmp.c \
    udpsocket/udpsocket.cpp \
    mongoose/mongoose.c \
    common/common.cpp \
    audio_g711.cpp \
    signalsource.cpp \
    hi3520dv300_comm_fun.cpp \
    cJSON.c

HEADERS += \
    global.h \
    http_server.h \
    http_handler.h \
    hi3520dv300_comm_fun.h \
    himpp_master.h \
    loadbmp.h \
    software_config.h \
    udpsocket/networkinfo.h \
    udpsocket/udpsocket.h \
    mongoose/mongoose.h \
    common/common.h \
    audio_g711.h \
    rtspc/player_rtsp.h \
    signalsource.h \
    aacenc.h \
    aac_encode.h \
    cJSON.h

#定义编译选项
#DEFINES += __TINY_RTSP_SERVER__ #简易rtsp server
#DEFINES += __TRANS_CODING__    #是否启用转码功能（对视频流再编码）
