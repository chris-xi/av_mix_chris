#ifndef SIGNALSOURCE_H
#define SIGNALSOURCE_H

#include <map>
#include <vector>
#include "himpp_master.h"
#include "player_rtsp.h"

using namespace std;

class SignalSource;
typedef map<string, SignalSource*> SSMap;

class SignalSource : player_media
{
public:
    struct VideoCache{
        char* data;
        unsigned int len;
    };

    SignalSource();
    ~SignalSource();

    bool PlayNetStream(const char *uri, const int vdchn, string protocol = "");
    bool CloseNetStream(string protocol = "");
    bool OpenWindow(HI_S32 u32VdChn, HI_U32 u32VoChn, RECT_S stRect, HI_U32 u32Priority = 100);
    bool CloseWindow(HI_U32 u32VoChn);

    map<HI_U32, HI_U32> GetVoChnVector();
    HI_U32 GetVdChn();
#ifdef _VIDEO_CACHE_
    static HI_VOID* VideoCacheDecodeProc(HI_VOID *__this);
#endif

public:
    virtual void on_video( void* data, unsigned int len, unsigned int pts, unsigned wnd );
    virtual void on_audio( void* data, unsigned int len, unsigned int pts, unsigned wnd );

private:
    bool DecodeVideo(void* data, unsigned int len, unsigned int pts, unsigned wnd);

#ifdef _VIDEO_CACHE_
    bool VideoCache(void* data, unsigned int len, unsigned int pts, unsigned wnd);
    bool PushVideo(void* data, unsigned int len, unsigned int pts, unsigned wnd);
#endif // _VIDEO_CACHE_

private:
#ifdef _VIDEO_CACHE_
    vector<struct VideoCache> videocache_;
#endif
    map<HI_U32, HI_U32> tovochn_; //用于存放该信号源对应的vo通道号 <vochn, priority>
    HI_U32 vdchn_;
    string uri_;
    HI_BOOL b_audio_;
    HI_BOOL b_close_; // 是否被关闭
    player_rtsp_sp rtsp_sp_;
};

#endif // SIGNALSOURCE_H
