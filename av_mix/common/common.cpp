#include "common.h"

int c_netmsgid = -1;
int c_osdmsgid = -1;
int c_netmsgtype = 1;
int c_osdmsgtype = 2;
const char *c_netmsgpath = ".";
const char *c_osdmsgpath = "..";

/*
 * 功能： 创建进程间的消息队列
 * 输入： const char*
 * 返回： int: id号
 * 日期： 2018.07.07
 * 作者： zh.sun
 */
int CreateMessageQueue(const char *msgpath)
{
    //创建消息通道
    int msgid;
    int msgkey = ftok(msgpath, 'a');
    if(msgkey == -1){
        COMMON_PRT("ftok create error: %d", errno);
        return false;
    }

    //建立消息队列
    do{
        // 若无，则创建；若有，则返回已有文件值
        msgid = msgget(msgkey, 0666 | IPC_CREAT);
        if(msgid == -1){
            COMMON_PRT("msgget create error: %d", errno);

            if(msgctl(msgid, IPC_RMID, NULL) == -1){
                COMMON_PRT("msgctl delete error: %d", errno);
                return -1;
            }
        }
    }while (msgid < 0);

    return msgid;
}

/*
 * 功能： 销毁进程间的消息队列
 * 输入： int
 * 返回： bool
 * 日期： 2018.07.07
 * 作者： zh.sun
 */
bool DeleteMessageQueue(int msgid)
{
    //删除消息队列
    if(msgctl(msgid, IPC_RMID, NULL) == -1)
    {
        COMMON_PRT("msgctl(IPC_RMID) failed");
        return false;
    }

    return true;
}

// 按照指定字符分离string字符串
void SplitString(const std::string& s, std::vector<std::string>& v, const std::string& c)
{
    std::string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;

    while (std::string::npos != pos2){
        v.push_back(s.substr(pos1, pos2 - pos1));
        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }

    if (pos1 != s.length())
        v.push_back(s.substr(pos1));
}

int str2int(const std::string &string_temp)
{
    int dst_temp = 0;
    std::stringstream stream(string_temp);
    stream >> dst_temp;
    return dst_temp;
}

/*
 * 功能： 创建一个线程
 * 输入： void* (*pThreadFunc)(void*)： 线程指向的函数指针
 *       uint priority： 该线程的优先级，取值[0，99]。仅仅在SCHED_FIFO和SCHED_RR下有效，默认调度策率下无效
 *       int schedpolicy： 该线程的调度策率，有三种：
 * SCHED_OTHER：分时调度策略（默认的）
 * SCHED_FIFO： 实时调度策略，先到先服务。一旦占用cpu则一直运行，一直运行直到有更高优先级任务到达或自己放弃
 * SCHED_RR：   实时调度策略，时间片轮转。时间片用完，系统将重新分配时间片，并置于就绪队列尾
 *       bool b_detached：线程是否设置为分离模式，即不需要使用pthread_join()
 *       void *threadin：用于对线程函数作为输入使用的，值为null时，表示不需要输入参数
 * 返回： （pthread_t）创建成功的线程id号,
 * 日期： 2018.08.03
 * 作者： zh.sun
 */
pthread_t CreateThread(void* (*pThreadFunc)(void*), uint priority, int schedpolicy, bool b_detached, void *threadin)
{
    pthread_t tid;
    pthread_attr_t attr;
    struct sched_param  param;
    pthread_attr_init(&attr);
    param.sched_priority = priority; //优先级：1～99
    //pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);//要使优先级其作用必须要有这句话.表示使用在schedpolicy和schedparam属性中显式设置的调度策略和参数
    pthread_attr_setschedparam(&attr, &param);
    pthread_attr_setschedpolicy(&attr, schedpolicy);

    if(b_detached)
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED); //分离模式，无需的等待（pthread_join）之后才释放占用资源，而是返回后自动释放占用的资源

    if(pthread_create(&tid, &attr, pThreadFunc, threadin) < 0){
        return 0;
    }

    pthread_attr_destroy(&attr);

    //pthread_timedjoin_np(); //超时等待
    return tid;
}
