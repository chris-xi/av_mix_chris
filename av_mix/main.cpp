#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <linux/socket.h>
#include <linux/netlink.h>
#include <pthread.h>
#include <map>

#include "himpp_master.h"
#include "signalsource.h"
#include "software_config.h"
#include "http_handler.h"
#include "aacenc.h"


#define MAXDATA BUFF_SIZE


typedef SoftwareConfig Config; //把Class类SoftwareConfig的名称缩写以下

/*******************************************************************
* function : to process abnormal case
*******************************************************************/
void HandleSig(HI_S32 signo)
{
    if (SIGINT == signo || SIGTSTP == signo)
    {
        SAMPLE_COMM_SYS_Exit();
        COMMON_PRT("\033[0;31mprogram termination abnormally!\033[0;39m\n");
    }
    exit(-1);
}

/*****************************************
 * add function : separation information form client
 * 1-- s_str: information from clinet
 * 2-- c_str: command from clinet
 * 3-- d_str: data from client
 * **************************************/
int GetInfo(const char *s_str,char *c_str,char *d_str)
{
    char str1 = '(';
    char str2 = ')';
    char str3 = '[';
    char str4 = ']';
    while( *s_str && (*s_str++ != str1));
    while(*s_str && (*s_str != str2))
        *c_str++ = *s_str++;
    *c_str = '\0';
    while(*s_str && (*s_str++ !=str3));
    while(*s_str && (*s_str !=str4))
        *d_str++ = *s_str++;
    *d_str ='\0';
    return 0;
}

/***************************************
 * add function : check AicoderFormat \ AiBiteRate match
 **************************************/
bool checkMatch(AACENC_CONFIG *pconfig)
{
    bool r_match = false;
    if(pconfig->coderFormat == AACLC)
    {
        if(pconfig->sampleRate == 44100)
        {
            if((pconfig->bitRate >= 32000) && (pconfig->bitRate <= 320000))
            {
                return r_match = true;
            }
        }
        else if(pconfig->sampleRate == 48000)
        {
            if((pconfig->bitRate >= 48000) &&  (pconfig->bitRate <= 320000))
            {
                return r_match = true;
            }
        }
        else
        {
            return r_match;
        }
    }
    else if(pconfig->coderFormat == EAAC)
    {
        if(pconfig->sampleRate == 44100)
        {
            if((pconfig->bitRate >= 24000) && (pconfig->bitRate <= 51000))
            {
                return r_match = true;
            }
        }
        else if(pconfig->sampleRate == 48000)
        {
            if((pconfig->bitRate >= 24000) && (pconfig->bitRate <= 51000))
            {
                return r_match = true;
            }
        }    else
        {
            return r_match;
        }
    }
    else if(pconfig->coderFormat == EAACPLUS)
    {
        if(pconfig->sampleRate == 44100)
        {
            if((pconfig->bitRate >= 10000) &&  (pconfig->bitRate <= 17000))
            {
                return r_match = true;
            }
        }
        else if(pconfig->sampleRate == 48000)
        {
            if((pconfig->bitRate >= 12000) &&  (pconfig->bitRate <= 35000))
            {
                return r_match = true;
            }
        }
        else
        {
            return r_match;
        }
    }
    return r_match;
}

/****************************************
 * add function :check parameter and save configure
 * 1--cmd:parameter
 * 2--info:value
 * 3--config:coderformat ,bitrate, samplerate
 **************************************/
bool checkSaveConfigure(const char *c_commd,const char *info,AACENC_CONFIG *config,UDPSocket *udpsocket,int *flag)
{
    string value;
    value.clear();
    value = info;
    auto thisini = Singleton<Config>::getInstance();

    if(!strcmp(c_commd,"IP")){
        thisini->SetConfig(Config::kIp,value);
        thisini->SaveConfig();
        string udpSvRtspUri = "rtsp://";
        udpSvRtspUri += (value + "/0");
        thisini->SetConfig(Config::kSvRtspUri,udpSvRtspUri);
        thisini->SaveConfig();
        *flag += 1;
    }
    else if(!strcmp(c_commd,"MASK")){
        thisini->SetConfig(Config::kMask,value);
        thisini->SaveConfig();
        *flag += 1;
    }
    else if(!strcmp(c_commd,"GATAWAY")){
        thisini->SetConfig(Config::kGateway,value);
        thisini->SaveConfig();
        *flag +=1;
    }
    else if(!strcmp(c_commd,"UDPSVPORT")){
        thisini->SetConfig(Config::kUdpSvPort,value);
        thisini->SaveConfig();
        *flag +=1;
    }
    else if(!strcmp(c_commd,"RTSPURI")){
        thisini->SetConfig(Config::kRtspUri,value);
        thisini->SaveConfig();
        *flag +=1;
    }
    else if(!strcmp(c_commd,"AISAMPLERATE")){
            thisini->SetConfig(Config::kAiSampleRate,value);
            thisini->SaveConfig();
            if(!value.compare("8000")){
                config->sampleRate = 8000;
                *flag += 1;
            }
            else if(!value.compare("44100")){
                config->sampleRate = 44100;
                *flag += 1;
            }
            else if(!value.compare("48000")){
                config->sampleRate = 48000;
                *flag += 1;
            }
            else
                udpsocket->EchoTo("errorcode1",strlen("errorcode1"));    //error code "1" ----aiSampleRate Error
        }
        else if(!strcmp(c_commd,"AICODERFORMAT")){

            if(!value.compare("G711A")){
                thisini->SetConfig(Config::kAiCoderFormat,"3");
                thisini->SaveConfig();
                config->coderFormat = G711A;
                *flag += 1;
            }
            else if(!value.compare("AACLC")){
                thisini->SetConfig(Config::kAiCoderFormat,"0");
                thisini->SaveConfig();
                config->coderFormat = AACLC;
                *flag += 1;
            }
            else if(!value.compare("EAAC")){
                thisini->SetConfig(Config::kAiCoderFormat,"1");
                thisini->SaveConfig();
                config->coderFormat = EAAC;
                *flag += 1;
            }
            else if(!value.compare("EAACPLUS")){
                thisini->SetConfig(Config::kAiCoderFormat,"2");
                thisini->SaveConfig();
                config->coderFormat = EAACPLUS;
                *flag += 1;
            }
            else
                udpsocket->EchoTo("errorcode2",strlen("errorcode2"));    //error code "2" ----aiCoderFormat Error
        }
    else if(!strcmp(c_commd,"AIBITRATE")){
        thisini->SetConfig(Config::kAiBitRate,value);
        thisini->SaveConfig();
        config->bitRate = str2int(value);
        *flag += 1;
    }
    else{
        udpsocket->EchoTo("errorcode3",strlen("errorcode3")); //error code "3" ----unknow parameter
    }
    return true;
}

/*****************************************
 * funcion : udpserver
 ****************************************/
void* udpServerProc(void *arg)
{
    printf("udpserver thread open\n");
    auto thisini = Singleton<Config>::getInstance();
    int  len = 0;
    int  c_re = 0;
    uint port = *(uint*)arg;
    char data[MAXDATA]  = "";
//   char reply[MAXDATA] = "";
    char c_commd[MAXDATA] = "";
    char c_value[MAXDATA] = "";
    AACENC_CONFIG config;
    int checkFlag = 0;
    COMMON_PRT("starting udp server at port: %d\n", port);

    memset(&config,0,sizeof(config));
    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(port, false);
    printf("udpsv mcsocket creage ok\n");

    while( 1 ){
        memset(data, '\0', sizeof(data));
        if( (len = udpsocket->RecvFrom(data, MAXDATA)) > 0){
            COMMON_PRT("recv udp data: %s\n", data);
            if(!strcmp(data, "RSET:")){ //重启指令
                udpsocket->EchoTo("(OK)", strlen("(OK)"));
                system("reboot");
                break;
            }
            else
            {
                if(0 == GetInfo(data,c_commd,c_value)){
                    if(true ==checkSaveConfigure(c_commd,c_value,&config,udpsocket,&checkFlag)){
                        printf("config write ok\n");
                    }
                    else
                        break;
                }
                else
                    printf("data error\n");
                if(8 == checkFlag){
                    if(true == checkMatch(&config)){
                        printf("all parameter ok\n");
                        udpsocket->EchoTo("code0",strlen("code0")); //"0"----set parameter ok,client get svrtspuri rtsp://ip/0
                        thisini->SaveConfig();
                        break;
                    }
                    else
                    {
                        printf("parameter not match\n");
                        udpsocket->EchoTo("errorcode4",strlen("errorcode4"));  //error code "4"----parameter not match
                        checkFlag = 0;           //client only reset Aisamplerate  Aicoderformat Aibitrate
                    }
                }
                else
                    printf("data less\n");
            }
        }
        else
            break;
    }
    delete udpsocket;
    return &(c_re = -1);
}







uint GetSystemInfo(char *buf_send)
{
    char localip[20] = "";
    char ver[20] = "";
    uint delay = 0;
    // 读取版本号
    FILE *f = fopen("./dog.sh", "r");
    if ( f ){
        fseek(f, strlen("#!/bin/sh\n#"), SEEK_SET);
        fscanf(f, "%s", ver);
        ver[strlen(ver)] = '\0'; //COMMON_PRT("version: %s\n", ver);
    }
    else{
        sprintf(ver, "%s", "where is dog.sh?");
        COMMON_PRT("open %s failed!\n", "dog.sh");
    }
    if ( f ){
        fclose(f);
        f = nullptr;
    }

    // 获取本地ip地址
    NetworkInfo *netinfo = new NetworkInfo();
    if( !netinfo->GetNetworkInfo(ETH0, NetworkInfo::kADDR, localip) ){
        COMMON_PRT("Get ip failed!\n");
        return 0;
    }
    string timer( localip );
    // 取得ip段的最后一位
    timer = timer.c_str() + timer.find(".", timer.find(".", timer.find(".") + 1) + 1) + strlen(".");
    // 根据ip地址计算回复延迟
    delay = atoi(timer.c_str()) * 1000; //COMMON_PRT("delay = %d us\n", delay);
    usleep(delay);

    sprintf(buf_send, "BKIP:%s,<A>%s,rtsp://%s/0", localip, ver,localip);

    return delay;
}


/*
 * 功能： 启动广播监听，并回复ip和版本号，扫描和回复格式兼容hi3536输出板子的扫描。
 * 回复 BKIP:xx.xx.xx.xx,<A>ver:x.x.x.x,rtsp://ip/0
 * 输入： void *arg：指定监听端口
 * 返回： 无
 * 日期： 2018.08.03
 * 作者： zh.sun
 */

void *udpBroadCastProc(void *arg)
{
    printf("broadcast thread open\n");
    int  len = 0;
    uint port = *(uint*)arg;
    char buf[MAXDATA] = "";
    COMMON_PRT("starting broadcast listened at port: %d\n", port);

    UDPSocket *broadsocket = new UDPSocket();
    broadsocket->CreateUDPServer(port, false); //设为阻塞
    printf("broadcaset mcsocket creage ok\n");
#if 0
    broadsocket->AddBroadCast(ETH0); //加入eth0网卡的广播地址
#else
    broadsocket->AddBroadCast(); //使用255.255.255.255广播地址
    broadsocket->BindLocalAddr(ETH0);
#endif

    while(1){
        memset(buf, '\0', sizeof(buf));
        if( (len = broadsocket->RecvFrom(buf, MAXDATA)) > 0){
            buf[len] = '\0'; COMMON_PRT("recv data: %s\n", buf);

            // 捕获节点扫描信号
            if(strstr(buf, "GTIP:") != nullptr){
                char buf_send[128] = "";
                uint delay = 0;
                delay = GetSystemInfo(buf_send);
                if(delay == 0){
                    COMMON_PRT("get system info failed!\n");
                }
                COMMON_PRT(">>>send data: %s\n", buf_send);
                // UDP回声回复
                               if(strlen(buf) == strlen("GTIP:")){
                                   for (uint i = 0; i < 3; i++){
                                       broadsocket->EchoTo(buf_send,
                                                           strlen(buf_send));
                                       usleep(delay/10);
                                   }
                               }
                                   else{
                                                   struct sockaddr_in clientaddr = broadsocket->GetClientSockaddr();
                                                   for (uint i = 0; i < 3; i++){
                                                       broadsocket->SendTo(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr),
                                                                           atoi(buf+5),
                                                                           buf_send,
                                                                           strlen(buf_send));
                                                       usleep(delay/10);
                                                   }
                                               }
                                           }
                                       }
                                   }

                                   delete broadsocket;

                                   return (void*)1;
               }




/*
 * 功能： 启动组播监听，并回复ip和版本号，扫描和回复格式兼容hi3536输出板子的扫描。
 * 回复 BKIP:xx.xx.xx.xx,<A>ver:x.x.x.x
 * 输入： void *arg：指定监听端口
 * 返回： 无
 * 日期： 2018.08.28
 * 作者： zh.sun
 */
void *udpMultiCastProc(void *arg)
{
    printf("multicaset thread open\n");
    uint port = *(uint*)arg;
    char mucip[32] = "239.255.255.250";
    int  len = 0;
    char buf[128];
    COMMON_PRT("starting multicast listened at port: %d\n", port);

    UDPSocket *mcsocket = new UDPSocket();
    mcsocket->CreateUDPServer(port, false); //设为阻塞
    printf("muticaset mcsocket creage ok\n");
    mcsocket->AddMemberShip(mucip, ETH0);

    while(1)  {
        memset(buf, '\0', sizeof(buf));
        if( (len = mcsocket->RecvFrom(buf, MAXDATA)) > 0){
            buf[len] = '\0'; //COMMON_PRT("recv data: %s\n", buf);

            // 捕获节点扫描信号
            if(strstr(buf, "GTIP:") != nullptr){
                char buf_send[128] = "";
                uint delay = 0;
                delay = GetSystemInfo(buf_send);
                if(delay == 0){
                    COMMON_PRT("get system info failed!\n");
                }
                COMMON_PRT(">>>send data: %s\n", buf_send);
                // UDP回声回复
                               if(strlen(buf) == strlen("GTIP:")){
                                   for (uint i = 0; i < 3; i++){
                                       mcsocket->EchoTo(buf_send,
                                                        strlen(buf_send));
                                       usleep(delay/10);
                                   }
                               }
                               else{
                                   struct sockaddr_in clientaddr = mcsocket->GetClientSockaddr();
                                   for (uint i = 0; i < 3; i++){
                                       mcsocket->SendTo(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr),
                                                        atoi(buf+5),
                                                        buf_send,
                                                        strlen(buf_send));
                                       usleep(delay/10);
                                   }
                               }
                           }
                       }
                   }

                   delete mcsocket;
                   return (void*)1;
               }


void SetSysNet(const char *ip, const char *mask, const char *gateway)
{
    char cmd[1024]={0};
    // set ip and netmask
    memset( cmd, 0, 1024 );
    sprintf( cmd, "ifconfig eth0 %s netmask %s\n", ip, mask );
    system ( cmd );
    printf( "%s", cmd );
    // del gateway
    memset( cmd, 0, 1024 );
    sprintf( cmd, "route del default gw %s dev eth0\n", "0.0.0.0");
    system( cmd );
    printf( "%s", cmd );
    // set gateway
    memset ( cmd, 0, 1024 );
    sprintf( cmd, "route add default gw %s dev eth0\n", gateway);
    system ( cmd );
    printf( "%s", cmd );
    sleep(1);

    return (void)0;
}

void *httpServerhandler(void*)
{
    // start http server
    auto thisini = Singleton<Config>::getInstance();
    auto http_server = Singleton<HttpServer>::getInstance();
    http_server->Init( thisini->GetConfig(Config::kWebPort) );
    // add http handler
    http_server->AddHandler("/reset",       ResetHandler);
    http_server->AddHandler("/updata",      UpdataHandler);
    http_server->AddHandler("/transmit",    TransmitHandler);
    http_server->AddHandler("/perplan",     PerplanHandler);
    http_server->AddHandler("/setnet",      SetnetHandler);
    http_server->AddHandler("/setaudio",    SetaudioHandler);

    http_server->AddHandler("/get_perplan",       GetperplanHandler);
    http_server->AddHandler("/get_netsetting",    GetnetsettingHandler);
    http_server->AddHandler("/get_audiosetting",  GetaudiosettingHandler);
    http_server->Start();

    return (void*)1;
}


int main()
{
    // 1. 挂载系统中断信号，检测Ctrl+C按键
    signal(SIGINT, HandleSig);

    // 2. 创建读取配置文件的单例
    auto thisini = Singleton<Config>::getInstance();
    thisini->ReadConfig();
    thisini->PrintConfig();
    thisini->SaveConfig();

    // 3. 读取预设参数
#ifdef __TRANS_CODING__
    VENC_CHN VencChn = 0;
    VDEC_CHN VdecChn = 0;
    HI_U32 u32VdCnt = 1;
    HI_S32 s32VpssGrpCnt = 1;
    HI_U32 u32SrcFrmRate = 60;
    PIC_SIZE_E enMaxPicSize = PIC_HD1080; //设定vi采集，vpss处理，vdec解码的最大分辨率
    HI_U32 u32Gop = str2int(thisini->GetConfig(Config::kGop));
    HI_U32 u32Profile = str2int(thisini->GetConfig(Config::kProfile));
    HI_U32 u32BitRate = str2int(thisini->GetConfig(Config::kBitRate));
    HI_U32 u32ViWidth  = str2int(thisini->GetConfig(Config::kVI_W));
    HI_U32 u32ViHeigth = str2int(thisini->GetConfig(Config::kVI_H));
    HI_U32 u32DstFrameRate = str2int(thisini->GetConfig(Config::kDstFrmRate));
    PAYLOAD_TYPE_E enType = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kVENC_Type));
    HI_U32 u32MinQp = str2int(thisini->GetConfig(Config::kMIN_QP));
    HI_U32 u32MaxQp = str2int(thisini->GetConfig(Config::kMAX_QP));
    HI_U32 u32IQp   = str2int(thisini->GetConfig(Config::kI_QP));
    HI_U32 u32PQp   = str2int(thisini->GetConfig(Config::kP_QP));
    if(u32ViWidth == 0) u32ViWidth = 1920;
    if(u32ViHeigth == 0) u32ViHeigth = 1080;
    SIZE_S stVencSize = {u32ViWidth, u32ViHeigth}; //编码分辨率，为方便固定为720P
#else
    VB_CONF_S stVbConf;
    SIZE_S stSize = {1920, 1080};
    SAMPLE_COMM_VDEC_Sysconf(&stVbConf, &stSize);
    HI_S32 s32Ret = SAMPLE_COMM_SYS_Init(&stVbConf);
    if (HI_SUCCESS != s32Ret){
        printf("%s: system init failed with %d!\n", __FUNCTION__, s32Ret);
        return HI_FAILURE;
    }
#endif //__TRANS_CODING__
    string rtsp_uri = thisini->GetConfig(Config::kRtspUri);
    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(Config::kAiSampleRate));

    // 4. 设置系统网络参数
    SetSysNet(string(thisini->GetConfig(Config::kIp)).c_str(),
              string(thisini->GetConfig(Config::kMask)).c_str(),
              string(thisini->GetConfig(Config::kGateway)).c_str());

    // 5. 启动 广播监听，10端口 的线程
        int broad_port = 10;
        bool b_detached = true;
        pthread_t broadcast_tid = CreateThread(udpBroadCastProc, 0, SCHED_FIFO, b_detached, &broad_port);
        if(broadcast_tid == 0){
            COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
        }
        // 5.1. 启动 组播监听，3700端口 的线程
        int multi_port = 3700;
        pthread_t multicast_tid = CreateThread(udpMultiCastProc, 0, SCHED_FIFO, b_detached, &multi_port);
        if(multicast_tid == 0){
            COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
        }
        //6.add mode---create  udp server thread


        int udp_port = atoi( thisini->GetConfig(Config::kUdpSvPort).c_str() );
        pthread_t udpserver_tid = CreateThread(udpServerProc, 0, SCHED_FIFO, b_detached, &udp_port);
        if(udpserver_tid == 0){
            COMMON_PRT("pthread_create() failed: net udpServerProc");
        }

   //6.启动 http Server 的线程
    //bool b_detached = true;
    pthread_t http_tid = CreateThread(httpServerhandler, 0, SCHED_FIFO, b_detached, NULL);
    if(http_tid == 0){
        COMMON_PRT("pthread_create() failed: httpserverhandler");
    }


    // 7. 创建HiMpp类的单例
    auto himppmaster = Singleton<HimppMaster>::getInstance();
#ifdef __TRANS_CODING__
    himppmaster->InitHimpp(s32VpssGrpCnt, enType, enMaxPicSize);
    himppmaster->VpssModuleInit();
    himppmaster->VencModuleInit(VencChn, stVencSize, u32Profile,
                                u32Gop, u32BitRate, u32SrcFrmRate,
                                u32DstFrameRate, u32MinQp, u32MaxQp,
                                u32IQp, u32PQp);
    himppmaster->VdecModuleInit(u32VdCnt);
//    himppmaster->VoModeuleInit(SAMPLE_VO_DEV_DHD0, SAMPLE_VO_LAYER_VHD0);
#endif //__TRANS_CODING__
    himppmaster->AiModeuleInit(enAiSampleRate);
    himppmaster->AencModeuleInit();
    himppmaster->ModeuleBind();

    // 8. rtsp server 启动
    char svrtspuri[50]={0};
    sprintf( svrtspuri, "rtsp://%s/0", string(thisini->GetConfig(Config::kIp)).c_str() );
    thisini->SetConfig(Config::kSvRtspUri, svrtspuri);
    thisini->SaveConfig();
#ifdef __TINY_RTSP_SERVER__
    g_prtspserver = new_server();
    while(g_prtspserver->start_rtsp_server( 554, "admin", "admin") != 0){
        printf("RTSP Server rebooting, wait for 10s\n");
        sleep(10);
    }
    g_stream = g_prtspserver->open_live_source( VIDEO_H264, AUDIO_G711, 1, (unsigned int)enAiSampleRate, "/0" );
#else
    if(crtsps_start((short)554) < 0){
        printf("rtsp server start failed!\n");
        return -1;
    }
    int param[2] = { 1, (int)enAiSampleRate };
    g_stream = crtsps_openstream( VIDEO_H264,
                                  AUDIO_AAC,
                                  "/0",
                                  (void*)&param );
#endif // __TINY_RTSP_SERVER__

    // 9. 开始编码并发送音频
	AuEncoderFormat coderFormat = (AuEncoderFormat)str2int(thisini->GetConfig(Config::kAiCoderFormat));
	if(coderFormat == 3){
		g711_init(); // g711a dec init
	}else{
        AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(Config::kAiSampleRate));
		HI_S32 bitRate = str2int(thisini->GetConfig(Config::kAiBitRate));
        g_audio_aac = new aacenc();
        g_audio_aac->open(bitRate, (HI_S32)enAiSampleRate, coderFormat);
	}

    himppmaster->StartAudioEnc();

    // 10. 开始接入远端rtsp流
    auto ss = Singleton<SignalSource>::getInstance();
    HI_S32 wnd = 0;
    ss->PlayNetStream(rtsp_uri.c_str(), wnd, "rtsp");

    // 11. 开始视频输入-编解码-视频输出的循环
#ifndef __TRANS_CODING__
    while(1){
        sleep(100);
    }
#else
    himppmaster->StartLoop();
#endif

    // 12. 销毁HiMpp开启的各个模块等
    g_audio_aac->close();
    delete g_audio_aac;
    g_audio_aac = NULL;
#ifdef __TRANS_CODING__
    himppmaster->VdecModuleDestroy();
    himppmaster->VencModuleDestroy();
//    himppmaster->VoModuleDestroy();
    himppmaster->VpssModuleDestroy();
#endif //__TRANS_CODING__
    himppmaster->AudioModuleDestroy();

    return 1;
}
