#include "signalsource.h"

SignalSource::SignalSource()
{
    rtsp_sp_ = make_shared<player_rtsp>();

    b_audio_ = HI_FALSE;
    b_close_ = HI_TRUE;
}

SignalSource::~SignalSource()
{
    auto iter = tovochn_.begin();
    while(iter != tovochn_.end()){
        this->CloseWindow(iter->first);
        iter++;
    }

    if(!b_close_)
        this->CloseNetStream();

    COMMON_PRT("~SignalSource\n");
}

void SignalSource::on_video( void* data, unsigned int len, unsigned int pts, unsigned wnd )
{
#ifndef __TRANS_CODING__
    //TODO：是否使用本地时间戳由外部参数配置
    HI_U64 u64CurPts;
    HI_MPI_SYS_GetCurPts(&u64CurPts);
    pts = u64CurPts/1000;
#ifdef __TINY_RTSP_SERVER__
    g_prtspserver->push_live_video_data( g_stream, data, len, pts );
#else
    crtsps_pushvideo( g_stream, (void*)data, len, true, pts );
#endif //__TINY_RTSP_SERVER__
    COMMON_PRT("<video>len: %d, pts: %d\n", len, pts);
#else
#ifdef _VIDEO_CACHE_
    VideoCache(data, len, pts, wnd);
#else
    DecodeVideo(data, len, pts, wnd);
#endif // _VIDEO_CACHE_
#endif //__TRANS_CODING__
}

void SignalSource::on_audio( void* data, unsigned int len, unsigned int pts, unsigned wnd )
{
    if(b_audio_){
#ifdef __TINY_RTSP_SERVER__
        g_prtspserver->push_live_audio_data( g_stream, data, len, pts );
#else
        crtsps_pushaudio( g_stream, (void*)data, len, pts );
#endif //__TINY_RTSP_SERVER__
        COMMON_PRT("<r - audio>len: %d, pts: %d\n", len, pts);
    }
}

bool SignalSource::PlayNetStream(const char *uri, const int vdchn, string protocol)
{
    //TO DO: 最好能加上一个uri的合法性检测，防止格式非法导致后续出现问题
    if(protocol == "rtsp"){
        rtsp_sp_->open(uri, vdchn, this);
    }
    else{
        COMMON_PRT("protocol is Invalid format.");
        return false;
    }

#ifdef _VIDEO_CACHE_
    /* 如果开启缓存机制（加强同步）则回启动该线程，目前处于测试版 */
    pthread_t tid;
    pthread_attr_t attr;
    struct sched_param  param;
    pthread_attr_init(&attr);
    param.sched_priority = 80; //优先级：1～99
    //pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);//要使优先级其作用必须要有这句话.表示使用在schedpolicy和schedparam属性中显式设置的调度策略和参数
    pthread_attr_setschedparam(&attr, &param);
    pthread_attr_setschedpolicy(&attr, SCHED_RR);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED); //分离模式，无需的等待（pthread_join）之后才释放占用资源，而是返回后自动释放占用的资源
    pthread_create(&tid, &attr, VideoCacheDecodeProc, (void*)this);
    pthread_attr_destroy(&attr);
#endif // _VIDEO_CACHE_

    vdchn_   = vdchn;
    b_close_ = HI_FALSE;

    return true;
}

bool SignalSource::CloseNetStream(string protocol)
{
    if(protocol == "rtsp"){
        rtsp_sp_->close();
    }
    else{
        COMMON_PRT("protocol is Invalid format.");
        return false;
    }

    b_close_ = HI_TRUE;
#ifdef _VIDEO_CACHE_
    gbPlay = HI_FALSE;
#endif // _VIDEO_CACHE_

    return true;
}

/*
 * 功能： 设置vo窗口的size或crop参数，并将其打开
 * 输入： HI_U32 u32VoChn： 通道号
 *       HI_S32 s32X： 左上角的x坐标
 *       HI_S32 s32Y： 左上角的y坐标
 *       HI_U32 u32Width： 宽度
 *       HI_U32 u32Height： 高度
 *       HI_U32 u32Priority： 图层优先级。
 *                           若该参数大于1，则表示对crop进行设置；
 *                           若该参数为0或者1，则表示对size进行设置
 * 返回： bool
 * 日期：2018.1.31
 * 作者： zh.sun
 */
bool SignalSource::OpenWindow(HI_S32 u32VdChn, HI_U32 u32VoChn, RECT_S stRect, HI_U32 u32Priority)
{
    // 设置Vo的大小
    if( u32Priority <= 1 ){
        if( HI_SUCCESS != Singleton<HimppMaster>::getInstance()->SetVoSize(u32VoChn, stRect, u32Priority))
            return false;

        //在map中查找指定元素
        if(tovochn_.find(u32VoChn) == tovochn_.end()){ //如果没有查找到对应的元素
            COMMON_PRT("vochn: %d, vdchn: %d", u32VoChn, u32VdChn);
            tovochn_.insert(make_pair(u32VoChn, u32Priority));

            //Singleton<HimppMaster>::getInstance()->SetVpssGrpFrameRate(u32VoChn, 60, 59);
            //            Singleton<HimppMaster>::getInstance()->VdecBindVpss(u32VdChn, u32VoChn);
            //            Singleton<HimppMaster>::getInstance()->VoBindVpss(u32VoChn, u32VoChn, u32Priority);
        }
    }

    return true;
}

bool SignalSource::CloseWindow(HI_U32 u32VoChn)
{
    //在map中查找指定元素
    auto iter = tovochn_.find(u32VoChn);
    //删除指定元素
    if(iter != tovochn_.end()){
        Singleton<HimppMaster>::getInstance()->DisableVo(u32VoChn, iter->second);
        //        Singleton<HimppMaster>::getInstance()->VdecUnbindVpss(u32VdChn, u32VoChn);
        tovochn_.erase(iter);
    }

    return true;
}

bool SignalSource::DecodeVideo(void* data, unsigned int len, unsigned int pts, unsigned wnd)
{
    VDEC_STREAM_S stStream;
    stStream.pu8Addr = (HI_U8*)(data);
    stStream.u32Len  = (HI_U32)(len );
    stStream.u64PTS  = pts;
    stStream.bEndOfStream = HI_FALSE;
    HI_S32 s32MilliSec = 0;
    /*当发完所有码流后，把 bEndOfStream 置为 1，表示码流文件结束，这时解码器会
     * 解完发送下来的所有码流并输出所有图像。如果发完所有码流后把 bEndOfStream
     * 置为 0，解码器内部可能残余大于等于一帧的图像未解码输出，因为解码器必须等
     * 到下一帧码流到来才能知道当前帧已经结束，送入解码。*/

#ifdef __DEBUG__
    if(!gbSendStream)
        return true;
#endif //__DEBUG__

    HI_S32 s32Ret = HI_MPI_VDEC_SendStream( wnd, &stStream, s32MilliSec );
    if( HI_SUCCESS != s32Ret ){
        COMMON_PRT("HI_MPI_VDEC_SendStream[%d] failed errno 0x%x\n", wnd, s32Ret);
        return HI_FALSE;
    }

#ifdef __DEBUG__
    /* 打印解码器信息 */
    VDEC_CHN_STAT_S pstStat;
    if( HI_SUCCESS == HI_MPI_VDEC_Query(wnd, &pstStat)){
        COMMON_PRT( "u32LeftStreamBytes:%d u32LeftStreamFrames:%d u32LeftPics:%d\n", pstStat.u32LeftStreamBytes, pstStat.u32LeftStreamFrames, pstStat.u32LeftPics );
    }
#endif //__DEBUG__

#if 0
    /*
     * 手动推送frame： vdec->(vpss-vo)
    */
    while( 1 )
    {
        VIDEO_FRAME_INFO_S info;
        s32Ret = HI_MPI_VDEC_GetImage( wnd, &info, s32MilliSec);
        if( s32Ret != HI_SUCCESS ) break;

        auto iter = tovochn_.begin();
        while(iter != tovochn_.end()){
            COMMON_PRT("vochn: %d", iter->first);
            if( HI_SUCCESS != HI_MPI_VPSS_SendFrame( iter->first, &info, s32MilliSec ) )
                COMMON_PRT( "HI_MPI_VPSS_UserSendFrame failed with %#x!\n", s32Ret );

            iter++;
        }

        HI_MPI_VDEC_ReleaseImage(wnd, &info);
    }
#endif

    return true;
}

#ifdef _VIDEO_CACHE_
bool SignalSource::VideoCache(void* data, unsigned int len, unsigned int pts, unsigned wnd)
{
    struct VideoCache vCache;
    vCache.data = (char*)data;
    vCache.len  = len;
    videocache_.push_back(vCache); //注意：这里这种push_back进去的数据时错误的，如果要使用则需要进一步修改
    //COMMON_PRT("videocache_ size: %d", videocache_.size());

    return HI_TRUE;
}

HI_VOID* SignalSource::VideoCacheDecodeProc(HI_VOID *__this)
{
    SignalSource *_this = (SignalSource *)__this;

    while(!gbPlay){
        usleep(10);
    }

    while(gbPlay){
        if(_this->videocache_.size() >= 1){
            COMMON_PRT("videocache_ size: %d", _this->videocache_.size());
            _this->DecodeVideo((void*)_this->videocache_[0].data, _this->videocache_[0].len, 0, 0);
            _this->videocache_.erase(_this->videocache_.begin());
        }
        else
            usleep(1000);
    }

    _this->videocache_.clear();

    return (HI_VOID*)1;
}
#endif // _VIDEO_CACHE_

map<HI_U32, HI_U32> SignalSource::GetVoChnVector()
{
    return tovochn_;
}

HI_U32 SignalSource::GetVdChn()
{
    return vdchn_;
}
