#ifndef _librtspc_i__
#define _librtspc_i__

class librtspc_callback_i
{
public:
    virtual void on_video( void* data, unsigned int len, unsigned int pts, unsigned wnd ) = 0;
    virtual void on_audio( void* data, unsigned int len, unsigned int pts, unsigned wnd ) = 0;
};

class librtspc_i
{
public:
    virtual int open_rtsp( const char * url, unsigned wnd, librtspc_callback_i* h ) = 0;
    virtual int close_rtsp( void ) = 0;
    virtual int get_videosize( int* w, int* h ) = 0;
};

// c++
extern "C" librtspc_i* new_librtspc( void );
extern "C" void delete_librtspc( librtspc_i* p );

#endif //_librtspc_i__
