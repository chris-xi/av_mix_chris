#ifndef PLAYER_RTSP_H
#define PLAYER_RTSP_H

#include <string>
#include <memory>  /* shared_ptr */
#include "librtspc_i.h"

/*
 * 功能： 接口过渡类，用于为rtsp和pte等协议解析出来的数据的处理提供对外的统一函数接口，外部调用时，重写on_video,on_audio两个函数即可
 * 输入：
 * 返回：
 * 日期： 2018.1.29
 * 作者： zh.sun
 */
class player_media
{
public:
    virtual void on_video( void* data, unsigned int len, unsigned int pts, unsigned wnd ) = 0;
    virtual void on_audio( void* data, unsigned int len, unsigned int pts, unsigned wnd ) = 0;
};


class player_rtsp : public librtspc_callback_i
{
public:
    player_rtsp()
    {
        m_play = new_librtspc();
        m_wnd = 0;
        mb_close = true;
    }
    ~player_rtsp()
    {
        if( !mb_close ){
            printf("close rtsp[wnd:%d]\n", m_wnd);
            m_play->close_rtsp();
        }
        delete_librtspc(m_play);
    }

    int open( const char* chost, unsigned wnd, player_media* om, bool block = true)
    {
        m_wnd = wnd;
        m_on_media = om;
        m_play->open_rtsp( chost, wnd, this );
        mb_close = false;
        return 0;
    }

    int close( void )
    {
        m_play->close_rtsp();
        mb_close = true;
        return 0;
    }

    int get_wnd()
    {
        return m_wnd;
    }

    int get_size( int* w, int* h )
    {
        m_play->get_videosize(w, h);
//        *w = 1920;
//        *h = 1080;
        return 0;
    }

    int get_video_codec( int* type )
    {
        *type = 96;
        return 0;
    }

public:
    virtual void on_video( void* data, unsigned int len, unsigned int pts, unsigned wnd )
    {
        m_on_media->on_video( data, len, pts, wnd );
    }
    virtual void on_audio( void* data, unsigned int len, unsigned int pts, unsigned wnd )
    {
        m_on_media->on_audio( data, len, pts, wnd );
    }

private:
    bool            mb_close;
    librtspc_i*     m_play;
    unsigned        m_wnd;
    player_media*   m_on_media;
};

typedef std::shared_ptr<player_rtsp> player_rtsp_sp;

#endif // PLAYER_RTSP_H
