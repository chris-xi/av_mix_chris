#ifndef AUDIO_G711_H
#define AUDIO_G711_H


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* Begin of #ifdef __cplusplus */

#include <stddef.h>

typedef unsigned char byte;

#define         SIGN_BIT        (0x80)      /* Sign bit for a A-law byte. */
#define         QUANT_MASK      (0xf)       /* Quantization field mask. */
#define         NSEGS           (8)         /* Number of A-law segments. */
#define         SEG_SHIFT       (4)         /* Left shift for segment number. */
#define         SEG_MASK        (0x70)      /* Segment field mask. */
#define         BIAS            (0x84)      /* Bias for linear code. */

int alaw2linear_user(unsigned char a_val);

void build_xlaw_table(byte *linear_to_xlaw,
                             int (*xlaw2linear)(unsigned char),
                             int mask);

void g711_init();

void g711_encode(byte * pcm, size_t pcm_size, byte * g711);

void g711_decode(byte * g711, size_t g711_size, byte * pcm);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */

#endif //AUDIO_G711_H
